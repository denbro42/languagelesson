<?php

// pull in config
require_once(dirname(__file__) . "/../../../../../config.php");
global $CFG;
// pull in the base test class
require_once($CFG->dirroot.'/mod/languagelesson/simpletest/action/base_action_test_class.php');

abstract class languagelesson_deletion_test_base extends languagelesson_page_actions_test {

    protected function delete_test($pageid) {
        $lesson = get_record('languagelesson', 'id', $this->TESTLLID);
        $deleter = new languagelesson_page_deleter($lesson);
        $deleter->delete_page($pageid);
    }

    public function setUp() {
        delete_records('languagelesson_pages', 'lessonid', $this->TESTLLID);
        delete_records('languagelesson_answers', 'lessonid', $this->TESTLLID);
        delete_records('languagelesson_branches', 'lessonid', $this->TESTLLID);
    }


}

?>
