<?php

// pull in config
require_once(dirname(__FILE__) . "/../../../../../config.php");
global $CFG;
// pull in the base test class
require_once($CFG->dirroot.'/mod/languagelesson/simpletest/action/delete/base_test_class.php');
// get the delete code
require_once($CFG->dirroot.'/mod/languagelesson/action/delete_class.php');

class languagelesson_delete_test_complex extends languagelesson_deletion_test_base {

    private $A;
    private $B;
    private $C;

    function __construct() {
        $this->TESTLLID = 50;
    }

    public function setUp() {
        parent::setUp();
        $this->A = $this->insert_question_page(null, 'A');
    }

    // cases

    // delete empty BT
    public function test_delete_empty_bt() {
        $bt = $this->insert_branch_table($this->A, array(0,0), 'BT');
        $this->delete_test($bt);
        list($A) = $this->validate_page_order(array($this->A));
        $this->validate_page_pointers($A, 0, null, 'A');

        $this->validate_branch_firstpages($bt, array());
    }

    // delete populated BT (1 question per branch)
    public function test_delete_populated_bt() {
        $bt = $this->insert_branch_table($this->A, array(0,0), 'BT');
        list($eob_ids, $branch_ids) = $this->get_bt_spawns($bt);
        $b = $this->insert_question_page($bt, 'B');
        $c = $this->insert_question_page($eob_ids[0], 'C');

        $this->delete_test($bt);
        list($A,$B,$C) = $this->validate_page_order(array($this->A, $b, $c));
        $this->validate_page_pointers($A, $B->id, null, 'A');
        $this->validate_page_pointers($B, $C->id, null, 'B');
        $this->validate_page_pointers($C, 0, null, 'C');

        $this->validate_branch_firstpages($bt, array());
    }

}

class languagelesson_delete_test_multiquestion_bt extends languagelesson_deletion_test_base {

    private $A;
    private $BT;
    private $B;
    private $C;
    private $D;
    private $E;

    function __construct() {
        $this->TESTLLID = 50;
    }

    public function setUp() {
        parent::setUp();
        $this->A = $this->insert_question_page(null, 'A');
        $this->BT = $this->insert_branch_table($this->A, array(0,0), 'BT');
        list($eob_ids, $branch_ids) = $this->get_bt_spawns($this->BT);
        $this->B = $this->insert_question_page($this->BT, 'B');
        $this->C = $this->insert_question_page($this->B, 'C');
        $this->D = $this->insert_question_page($this->C, 'D');
        $this->E = $this->insert_question_page($eob_ids[1], 'E');
    }

    // delete first question in branch
    public function test_delete_branch_first_question() {
        $this->delete_test($this->B);
        list($eob_ids, $branch_ids) = $this->get_bt_spawns($this->BT);
        list($A,$BT,$C,$D,$EOB1,$EOB2,$E) = $this->validate_page_order(array($this->A, $this->BT, $this->C, $this->D,
                                                                             $eob_ids[0], $eob_ids[1], $this->E));
        $this->validate_page_pointers($A, $BT->id, null, 'A');
        $this->validate_page_pointers($BT, $C->id, null, 'BT');
        $this->validate_page_pointers($C, $D->id, $branch_ids[0], 'C');
        $this->validate_page_pointers($D, $EOB1->id, $branch_ids[0], 'D');
        $this->validate_page_pointers($E, 0, null, 'E');

        $this->validate_branch_firstpages($this->BT, array($this->C, 0));
    }

    // delete last question in branch
    public function test_delete_branch_last_question() {
        $this->delete_test($this->C);
        list($eob_ids, $branch_ids) = $this->get_bt_spawns($this->BT);
        list($A,$BT,$B,$D,$EOB1,$EOB2,$E) = $this->validate_page_order(array($this->A, $this->BT, $this->B, $this->D,
                                                                             $eob_ids[0], $eob_ids[1], $this->E));
        $this->validate_page_pointers($A, $BT->id, null, 'A');
        $this->validate_page_pointers($BT, $B->id, null, 'BT');
        $this->validate_page_pointers($B, $D->id, $branch_ids[0], 'B');
        $this->validate_page_pointers($D, $EOB1->id, $branch_ids[0], 'D');
        $this->validate_page_pointers($E, 0, null, 'E');

        $this->validate_branch_firstpages($this->BT, array($this->B, 0));
    }

    // delete middle question in branch
    public function test_delete_branch_middle_question() {
        $this->delete_test($this->D);
        list($eob_ids, $branch_ids) = $this->get_bt_spawns($this->BT);
        list($A,$BT,$B,$C,$EOB1,$EOB2,$E) = $this->validate_page_order(array($this->A, $this->BT, $this->B, $this->C,
                                                                             $eob_ids[0], $eob_ids[1], $this->E));
        $this->validate_page_pointers($A, $BT->id, null, 'A');
        $this->validate_page_pointers($BT, $B->id, null, 'BT');
        $this->validate_page_pointers($B, $C->id, $branch_ids[0], 'B');
        $this->validate_page_pointers($C, $EOB1->id, $branch_ids[0], 'C');
        $this->validate_page_pointers($E, 0, null, 'E');

        $this->validate_branch_firstpages($this->BT, array($this->B, 0));
    }
    
}

class languagelesson_delete_test_nested_bts extends languagelesson_deletion_test_base {

    private $A;
    private $BT1;
    private $B;
    private $BT2;
    private $C;
    private $D;
    private $E;

    function __construct() {
        $this->TESTLLID = 50;
    }

    public function setUp() {
        parent::setUp();
        $this->A = $this->insert_question_page(null, 'A');
        $this->BT1 = $this->insert_branch_table($this->A, array(0,0), 'BT1');
        list($eob_ids1, $branch_ids1) = $this->get_bt_spawns($this->BT1);
        $this->B = $this->insert_question_page($this->BT1, 'B');
        $this->BT2 = $this->insert_branch_table($this->B, array(0,0), 'BT2');
        list($eob_ids2, $branch_ids2) = $this->get_bt_spawns($this->BT2);
        $this->C = $this->insert_question_page($this->BT2, 'C');
        $this->D = $this->insert_question_page($eob_ids2[1], 'D');
        $this->E = $this->insert_question_page($eob_ids1[0], 'E');
        $this->F = $this->insert_question_page($eob_ids1[1], 'F');
    }

    // delete nested BT
    public function test_delete_nested_bt() {
        $this->delete_test($this->BT2);
        list($eob_ids, $branch_ids) = $this->get_bt_spawns($this->BT1);
        list($A,$BT1,$B,$C,$D,$EOB1,$E,$EOB2,$F) = $this->validate_page_order(array($this->A, $this->BT1, $this->B, $this->C,
                                                                                    $this->D, $eob_ids[0], $this->E,
                                                                                    $eob_ids[1], $this->F));
        $this->validate_page_pointers($B, $C->id, $branch_ids[0], 'B');
        $this->validate_page_pointers($C, $D->id, $branch_ids[0], 'C');

        $this->validate_branch_firstpages($this->BT1, array($this->B, $this->E));
        $this->validate_branch_firstpages($this->BT2, array());
    }

    // delete containing BT
    public function test_delete_containing_bt() {
        $this->delete_test($this->BT1);
        list($eob_ids, $branch_ids) = $this->get_bt_spawns($this->BT2);
        list($A,$B,$BT2,$C,$EOB3,$EOB4,$D,$E,$F) = $this->validate_page_order(array($this->A, $this->B, $this->BT2, $this->C,
                                                                                    $eob_ids[0], $eob_ids[1], $this->D,
                                                                                    $this->E, $this->F));
        $this->validate_page_pointers($A, $B->id, null, 'A');
        $this->validate_page_pointers($B, $BT2->id, null, 'B');
        $this->validate_page_pointers($BT2, $C->id, null, 'BT2');
        $this->validate_page_pointers($C, $EOB3->id, $branch_ids[0], 'C');
        $this->validate_page_pointers($EOB4, $D->id, $branch_ids[1], 'EOB4');
        $this->validate_page_pointers($D, $E->id, null, 'D');
        $this->validate_page_pointers($E, $F->id, null, 'E');
        $this->validate_page_pointers($F, 0, null, 'F');

        $this->validate_branch_firstpages($this->BT1, array());
        $this->validate_branch_firstpages($this->BT2, array($this->C, 0));
    }

}

?>
