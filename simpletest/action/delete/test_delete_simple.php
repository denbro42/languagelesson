<?php
// pull in config
require_once(dirname(__FILE__) . "/../../../../../config.php");
global $CFG;
// pull in the base test class
require_once($CFG->dirroot.'/mod/languagelesson/simpletest/action/delete/base_test_class.php');
// get the delete code
require_once($CFG->dirroot.'/mod/languagelesson/action/delete_class.php');

// pull in insertion code
require_once($CFG->dirroot.'/mod/languagelesson/action/insert_class.php');


class languagelesson_delete_test_simple extends languagelesson_deletion_test_base {

    private $A;
    private $B;
    private $C;

    function __construct() {
        $this->TESTLLID = 50;
    }

    public function setUp() {
        parent::setUp();
        $this->A = $this->insert_question_page(null);
        $this->B = $this->insert_question_page($this->A);
        $this->C = $this->insert_question_page($this->B);
    }

    public function tearDown() {
    }

    // cases

    // delete the first page
    public function test_delete_first_page() {
        $this->delete_test($this->A);

        list($B,$C) = $this->validate_page_order(array($this->B, $this->C));
        $this->validate_page_pointers($B, $this->C, null, 'B');
        $this->validate_page_pointers($C, 0, null, 'C');
    }

    // delete the last page
    public function test_delete_last_page() {
        $this->delete_test($this->C);

        list($A,$B) = $this->validate_page_order(array($this->A, $this->B));
        $this->validate_page_pointers($A, $this->B, null, 'A');
        $this->validate_page_pointers($B, 0, null, 'B');
    }

    // delete the middle page
    public function test_delete_middle_page() {
        $this->delete_test($this->B);

        list($A,$C) = $this->validate_page_order(array($this->A, $this->C));
        $this->validate_page_pointers($A, $this->C, null, 'A');
        $this->validate_page_pointers($C, 0, null, 'C');
    }

}

?>
