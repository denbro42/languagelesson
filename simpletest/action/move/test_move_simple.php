<?php

// pull in config
require_once(dirname(__FILE__) . "/../../../../../config.php");
global $CFG;
// pull in the move test class
require_once($CFG->dirroot.'/mod/languagelesson/simpletest/action/move/base_test_class.php');



class languagelesson_move_test_simple extends languagelesson_move_test {
    private $A = 498;
    private $B = 499;
    private $C = 500;
    private $D = 501;

    function __construct() {
        $this->TESTLLID = 42;
    }

    public function setUp() {
        $pages = array(
                    array ( 'id' => $this->A,
                            'branchid' => null,
                            'prevpageid' => 0,
                            'nextpageid' => $this->B,
                            'ordering' => 1 ),
                    array ( 'id' => $this->B,
                            'branchid' => null,
                            'prevpageid' => $this->A,
                            'nextpageid' => $this->C,
                            'ordering' => 2 ),
                    array ( 'id' => $this->C,
                            'branchid' => null,
                            'prevpageid' => $this->B,
                            'nextpageid' => $this->D,
                            'ordering' => 3 ),
                    array ( 'id' => $this->D,
                            'branchid' => null,
                            'prevpageid' => $this->C,
                            'nextpageid' => 0,
                            'ordering' => 4 )
                );

        $this->genericSetUp($pages, array());
    }


    // base case
    public function test_swap_middle_pages() {
        $this->move_test($this->B, $this->C);
        list($A,$C,$B,$D) = $this->validate_page_order(array($this->A, $this->C, $this->B, $this->D));
        $this->validate_page_pointers($A, $this->C, null, 'A');
        $this->validate_page_pointers($C, $this->B, null, 'C');
        $this->validate_page_pointers($B, $this->D, null, 'B');
        $this->validate_page_pointers($D, 0, null, 'D');
    }

    // FIRSTPAGE cases
    public function test_new_firstpage() {
        $this->move_test($this->C, 0);
        list($C,$A,$B,$D) = $this->validate_page_order(array($this->C, $this->A, $this->B, $this->D));
        $this->validate_page_pointers($C, $this->A, null, 'C');
        $this->validate_page_pointers($A, $this->B, null, 'A');
        $this->validate_page_pointers($B, $this->D, null, 'B');
        $this->validate_page_pointers($D, 0, null, 'D');
    }

    public function test_move_firstpage() {
        $this->move_test($this->A, $this->C);
        list($B,$C,$A,$D) = $this->validate_page_order(array($this->B, $this->C, $this->A, $this->D));
        $this->validate_page_pointers($B, $this->C, null, 'B');
        $this->validate_page_pointers($C, $this->A, null, 'C');
        $this->validate_page_pointers($A, $this->D, null, 'A');
        $this->validate_page_pointers($D, 0, null, 'D');
    }

    public function test_swap_first_pages_ab() {
        $this->move_test($this->A, $this->B);
        list($B,$A,$C,$D) = $this->validate_page_order(array($this->B, $this->A, $this->C, $this->D));
        $this->validate_page_pointers($B, $this->A, null, 'B');
        $this->validate_page_pointers($A, $this->C, null, 'A');
        $this->validate_page_pointers($C, $this->D, null, 'C');
        $this->validate_page_pointers($D, 0, null, 'D');
    }

    public function test_swap_first_pages_ba() {
        $this->move_test($this->B, 0);
        list($B,$A,$C,$D) = $this->validate_page_order(array($this->B, $this->A, $this->C, $this->D));
        $this->validate_page_pointers($B, $this->A, null, 'B');
        $this->validate_page_pointers($A, $this->C, null, 'A');
        $this->validate_page_pointers($C, $this->D, null, 'C');
        $this->validate_page_pointers($D, 0, null, 'D');
    }

    // LASTPAGE cases
    public function test_new_lastpage() {
        $this->move_test($this->B, $this->D);
        list($A,$C,$D,$B) = $this->validate_page_order(array($this->A, $this->C, $this->D, $this->B));
        $this->validate_page_pointers($A, $this->C, null, 'A');
        $this->validate_page_pointers($C, $this->D, null, 'C');
        $this->validate_page_pointers($D, $this->B, null, 'D');
        $this->validate_page_pointers($B, 0, null, 'B');
    }

    public function test_move_lastpage() {
        $this->move_test($this->D, $this->A);
        list($A,$D,$B,$C) = $this->validate_page_order(array($this->A, $this->D, $this->B, $this->C));
        $this->validate_page_pointers($A, $this->D, null, 'A');
        $this->validate_page_pointers($D, $this->B, null, 'D');
        $this->validate_page_pointers($B, $this->C, null, 'B');
        $this->validate_page_pointers($C, 0, null, 'C');
    }

    public function test_swap_lastpages_dc() {
        $this->move_test($this->D, $this->B);
        list($A,$B,$D,$C) = $this->validate_page_order(array($this->A, $this->B, $this->D, $this->C));
        $this->validate_page_pointers($A, $this->B, null, 'A');
        $this->validate_page_pointers($B, $this->D, null, 'B');
        $this->validate_page_pointers($D, $this->C, null, 'D');
        $this->validate_page_pointers($C, 0, null, 'C');
    }

    public function test_swap_lastpages_cd() {
        $this->move_test($this->C, $this->D);
        list($A,$B,$D,$C) = $this->validate_page_order(array($this->A, $this->B, $this->D, $this->C));
        $this->validate_page_pointers($A, $this->B, null, 'A');
        $this->validate_page_pointers($B, $this->D, null, 'B');
        $this->validate_page_pointers($D, $this->C, null, 'D');
        $this->validate_page_pointers($C, 0, null, 'C');
    }

    // BOTH cases
    public function test_first_to_last() {
        $this->move_test($this->A, $this->D);
        list($B,$C,$D,$A) = $this->validate_page_order(array($this->B, $this->C, $this->D, $this->A));
        $this->validate_page_pointers($B, $this->C, null, 'B');
        $this->validate_page_pointers($C, $this->D, null, 'C');
        $this->validate_page_pointers($D, $this->A, null, 'D');
        $this->validate_page_pointers($A, 0, null, 'A');
    }

    public function test_last_to_first() {
        $this->move_test($this->D, 0);
        list($D,$A,$B,$C) = $this->validate_page_order(array($this->D, $this->A, $this->B, $this->C));
        $this->validate_page_pointers($D, $this->A, null, 'D');
        $this->validate_page_pointers($A, $this->B, null, 'A');
        $this->validate_page_pointers($B, $this->C, null, 'B');
        $this->validate_page_pointers($C, 0, null, 'C');
    }

}

?>
