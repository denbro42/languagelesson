<?php

// pull in config
require_once(dirname(__FILE__) . "/../../../../../config.php");
global $CFG;
// pull in the base test class
require_once($CFG->dirroot.'/mod/languagelesson/simpletest/action/base_action_test_class.php');
// get the move code
require_once($CFG->dirroot.'/mod/languagelesson/action/move_class.php');

abstract class languagelesson_move_test extends languagelesson_page_actions_test {

    protected function genericSetUp($pages, $branches) {
        foreach ($pages as $pagedata) {
            $page = new stdClass;
            $page->id = $pagedata['id'];
            $page->branchid = $pagedata['branchid'];
            $page->prevpageid = $pagedata['prevpageid'];
            $page->nextpageid = $pagedata['nextpageid'];
            $page->ordering = $pagedata['ordering'];

            if (! update_record('languagelesson_pages', $page)) {
                error ('Could not reset the test LL pages!');
            }
        }

        if ($branches) {
            foreach ($branches as $branchdata) {
                $branch = new stdClass;
                $branch->id = $branchdata['id'];
                $branch->firstpage = $branchdata['firstpage'];
                $branch->ordering = $branchdata['ordering'];
                if (! update_record('languagelesson_branches', $branch)) {
                    error ('Could not reset test LL branches!');
                }
            }
        }
    }


    protected function move_test($tomove, $after) {
        if (!$movepage = get_record("languagelesson_pages", "id", $tomove)) {
            error("Move: page not found");
        }

		$mover = new languagelesson_page_mover();
		$mover->lessonid = $this->TESTLLID;
		$mover->move($movepage, $after);
    }


}


?>
