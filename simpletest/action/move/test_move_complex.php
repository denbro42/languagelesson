<?php

// pull in config
require_once(dirname(__FILE__) . "/../../../../../config.php");
global $CFG;
// pull in the move test class
require_once($CFG->dirroot.'/mod/languagelesson/simpletest/action/move/base_test_class.php');

class languagelesson_move_test_complex extends languagelesson_move_test {

    private $A = 561;
    private $bt = 558;
    private $eob_1 = 559;
    private $eob_2 = 560;
    private $branch_1 = 176;
    private $branch_2 = 177;

    function __construct() {
        $this->TESTLLID = 47;
    }

    public function setUp() {
        $pages = array(
                    array ( 'id' => $this->A,
                            'branchid' => null,
                            'prevpageid' => 0,
                            'nextpageid' => $this->bt,
                            'ordering' => 1 ),
                    array ( 'id' => $this->bt,
                            'branchid' => null,
                            'prevpageid' => $this->A,
                            'nextpageid' => $this->eob_1,
                            'ordering' => 2 ),
                    array ( 'id' => $this->eob_1,
                            'branchid' => $this->branch_1,
                            'prevpageid' => $this->bt,
                            'nextpageid' => $this->bt,
                            'ordering' => 3 ),
                    array ( 'id' => $this->eob_2,
                            'branchid' => $this->branch_2,
                            'prevpageid' => $this->eob_1,
                            'nextpageid' => 0,
                            'ordering' => 4 )
                );
        $branches = array(
                        array ( 'id' => $this->branch_1,
                                'firstpage' => 0,
                                'ordering' => 1),
                        array ( 'id' => $this->branch_2,
                                'firstpage' => 0,
                                'ordering' => 2)
                    );

        $this->genericSetUp($pages, $branches);
    }



    public function tearDown() {
        $this->move_test($this->A, 0);
        list($A,$BT,$EOB1,$EOB2) = $this->validate_page_order(array($this->A, $this->bt, $this->eob_1, $this->eob_2));
        $this->validate_page_pointers($A, $this->bt, null, 'A');
        $this->validate_page_pointers($BT, $this->eob_1, null, 'BT');
        $this->validate_page_pointers($EOB1, $this->bt, $this->branch_1, 'EOB1');
        $this->validate_page_pointers($EOB2, 0, $this->branch_2, 'EOB2');

        $this->validate_branch_firstpages($this->bt, array(0, 0));
    }


    public function test_a_to_branch1() {
        // move A to be the first page of branch 1
        $this->move_test($this->A, $this->bt);
        // check results
        list($BT,$A,$EOB1,$EOB2) = $this->validate_page_order(array($this->bt, $this->A, $this->eob_1, $this->eob_2));
        $this->validate_page_pointers($BT, $this->A, null, 'BT');
        $this->validate_page_pointers($A, $this->eob_1, $this->branch_1, 'A');
        $this->validate_page_pointers($EOB1, $this->bt, $this->branch_1, 'EOB1');
        $this->validate_page_pointers($EOB2, 0, $this->branch_2, 'EOB2');
        
        $this->validate_branch_firstpages($this->bt, array($this->A, 0));
    }

    public function test_a_to_branch2() {
        // move A to be the first page of branch 2
        $this->move_test($this->A, $this->eob_1);
        // check results
        list($BT,$EOB1,$A,$EOB2) = $this->validate_page_order(array($this->bt, $this->eob_1, $this->A, $this->eob_2));
        $this->validate_page_pointers($BT, $this->eob_1, null, 'BT');
        $this->validate_page_pointers($EOB1, $this->bt, $this->branch_1, 'EOB1');
        $this->validate_page_pointers($A, $this->eob_2, $this->branch_2, 'A');
        $this->validate_page_pointers($EOB2, 0, $this->branch_2, 'EOB2');
        
        $this->validate_branch_firstpages($this->bt, array(0, $this->A));
    }

    public function test_a_to_end() {
        // move A to be the last page of the LL
        $this->move_test($this->A, $this->eob_2);
        // check results
        list($BT,$EOB1,$EOB2,$A) = $this->validate_page_order(array($this->bt, $this->eob_1, $this->eob_2, $this->A));
        $this->validate_page_pointers($BT, $this->eob_1, null, 'BT');
        $this->validate_page_pointers($EOB1, $this->bt, $this->branch_1, 'EOB1');
        $this->validate_page_pointers($EOB2, $this->A, $this->branch_2, 'EOB2');
        $this->validate_page_pointers($A, 0, null, 'A');
        $this->validate_branch_firstpages($this->bt, array(0, 0));
    }

}

class languagelesson_move_test_complex_b extends languagelesson_move_test {

    private $A = 562;
    private $bt = 563;
    private $B = 566;
    private $C = 567;
    private $eob_1 = 564;
    private $D = 568;
    private $eob_2 = 565;
    private $E = 569;
    private $branch_1 = 178;
    private $branch_2 = 179;

    function __construct() {
        $this->TESTLLID = 48;
    }

    public function setUp() {
        $pages = array(
                    array ( 'id' => $this->A,
                            'branchid' => null,
                            'prevpageid' => 0,
                            'nextpageid' => $this->bt,
                            'ordering' => 1 ),
                    array ( 'id' => $this->bt,
                            'branchid' => null,
                            'prevpageid' => $this->A,
                            'nextpageid' => $this->B,
                            'ordering' => 2 ),
                    array ( 'id' => $this->B,
                            'branchid' => $this->branch_1,
                            'prevpageid' => $this->bt,
                            'nextpageid' => $this->C,
                            'ordering' => 3 ),
                    array ( 'id' => $this->C,
                            'branchid' => $this->branch_1,
                            'prevpageid' => $this->B,
                            'nextpageid' => $this->eob_1,
                            'ordering' => 4 ),
                    array ( 'id' => $this->eob_1,
                            'branchid' => $this->branch_1,
                            'prevpageid' => $this->C,
                            'nextpageid' => $this->bt,
                            'ordering' => 5 ),
                    array ( 'id' => $this->D,
                            'branchid' => $this->branch_2,
                            'prevpageid' => $this->eob_1,
                            'nextpageid' => $this->eob_2,
                            'ordering' => 6 ),
                    array ( 'id' => $this->eob_2,
                            'branchid' => $this->branch_2,
                            'prevpageid' => $this->D,
                            'nextpageid' => $this->E,
                            'ordering' => 7 ),
                    array ( 'id' => $this->E,
                            'branchid' => null,
                            'prevpageid' => $this->eob_2,
                            'nextpageid' => 0,
                            'ordering' => 8 )
                );

        $branches = array(
                        array ( 'id' => $this->branch_1,
                                'firstpage' => $this->B,
                                'ordering' => 1),
                        array ( 'id' => $this->branch_2,
                                'firstpage' => $this->D,
                                'ordering' => 2)
                    );

        $this->genericSetUp($pages, $branches);
    }

    public function tearDown() {
        $this->move_test($this->A, 0);
        list($A,$BT,$B,$C,$EOB1,$D,$EOB2,$E) = $this->validate_page_order(array($this->A, $this->bt, $this->B, $this->C,
                                                                                $this->eob_1, $this->D, $this->eob_2,
                                                                                $this->E));
        $this->validate_page_pointers($A, $this->bt, null, 'A');
        $this->validate_page_pointers($BT, $this->B, null, 'BT');
        $this->validate_page_pointers($B, $this->C, $this->branch_1, 'B');
        $this->validate_page_pointers($C, $this->eob_1, $this->branch_1, 'C');
        $this->validate_page_pointers($EOB1, $this->bt, $this->branch_1, 'EOB1');
        $this->validate_page_pointers($D, $this->eob_2, $this->branch_2, 'D');
        $this->validate_page_pointers($EOB2, $this->E, $this->branch_2, 'EOB2');
        $this->validate_page_pointers($E, 0, null, 'E');

        $this->validate_branch_firstpages($this->bt, array($this->B, $this->D));
    }

    // A before B
    public function test_a_before_b() {
        $this->move_test($this->A, $this->bt);
        list($BT,$A,$B,$C,$EOB1,$D,$EOB2,$E) = $this->validate_page_order(array($this->bt, $this->A, $this->B,
                                                                                $this->C, $this->eob_1, $this->D,
                                                                                $this->eob_2, $this->E));
        $this->validate_page_pointers($BT, $this->A, null, 'BT');
        $this->validate_page_pointers($A, $this->B, $this->branch_1, 'A');
        $this->validate_page_pointers($B, $this->C, $this->branch_1, 'B');
        $this->validate_page_pointers($C, $this->eob_1, $this->branch_1, 'C');
        $this->validate_page_pointers($EOB1, $this->bt, $this->branch_1, 'EOB1');
        $this->validate_page_pointers($D, $this->eob_2, $this->branch_2, 'D');
        $this->validate_page_pointers($EOB2, $this->E, $this->branch_2, 'EOB2');
        $this->validate_page_pointers($E, 0, null, 'E');

        $this->validate_branch_firstpages($this->bt, array($this->A, $this->D));
    }
    // A after B
    public function test_a_after_b() {
        $this->move_test($this->A, $this->B);
        list($BT,$B,$A,$C,$EOB1,$D,$EOB2,$E) = $this->validate_page_order(array($this->bt, $this->B, $this->A,
                                                                                $this->C, $this->eob_1, $this->D,
                                                                                $this->eob_2, $this->E));
        $this->validate_page_pointers($BT, $this->B, null, 'BT');
        $this->validate_page_pointers($B, $this->A, $this->branch_1, 'B');
        $this->validate_page_pointers($A, $this->C, $this->branch_1, 'A');
        $this->validate_page_pointers($C, $this->eob_1, $this->branch_1, 'C');
        $this->validate_page_pointers($EOB1, $this->bt, $this->branch_1, 'EOB1');
        $this->validate_page_pointers($D, $this->eob_2, $this->branch_2, 'D');
        $this->validate_page_pointers($EOB2, $this->E, $this->branch_2, 'EOB2');
        $this->validate_page_pointers($E, 0, null, 'E');

        $this->validate_branch_firstpages($this->bt, array($this->B, $this->D));
    }
    // A after C
    public function test_a_after_c() {
        $this->move_test($this->A, $this->C);
        list($BT,$B,$C,$A,$EOB1,$D,$EOB2,$E) = $this->validate_page_order(array($this->bt, $this->B, $this->C,
                                                                                $this->A, $this->eob_1, $this->D,
                                                                                $this->eob_2, $this->E));
        $this->validate_page_pointers($BT, $this->B, null, 'BT');
        $this->validate_page_pointers($B, $this->C, $this->branch_1, 'B');
        $this->validate_page_pointers($C, $this->A, $this->branch_1, 'C');
        $this->validate_page_pointers($A, $this->eob_1, $this->branch_1, 'A');
        $this->validate_page_pointers($EOB1, $this->bt, $this->branch_1, 'EOB1');
        $this->validate_page_pointers($D, $this->eob_2, $this->branch_2, 'D');
        $this->validate_page_pointers($EOB2, $this->E, $this->branch_2, 'EOB2');
        $this->validate_page_pointers($E, 0, null, 'E');

        $this->validate_branch_firstpages($this->bt, array($this->B, $this->D));
    }
    // A before D
    public function test_a_before_d() {
        $this->move_test($this->A, $this->eob_1);
        list($BT,$B,$C,$EOB1,$A,$D,$EOB2,$E) = $this->validate_page_order(array($this->bt, $this->B, $this->C,
                                                                                $this->eob_1, $this->A, $this->D,
                                                                                $this->eob_2, $this->E));
        $this->validate_page_pointers($BT, $this->B, null, 'BT');
        $this->validate_page_pointers($B, $this->C, $this->branch_1, 'B');
        $this->validate_page_pointers($C, $this->eob_1, $this->branch_1, 'C');
        $this->validate_page_pointers($EOB1, $this->bt, $this->branch_1, 'EOB1');
        $this->validate_page_pointers($A, $this->D, $this->branch_2, 'A');
        $this->validate_page_pointers($D, $this->eob_2, $this->branch_2, 'D');
        $this->validate_page_pointers($EOB2, $this->E, $this->branch_2, 'EOB2');
        $this->validate_page_pointers($E, 0, null, 'E');

        $this->validate_branch_firstpages($this->bt, array($this->B, $this->A));
    }
    // A after D
    public function test_a_after_d() {
        $this->move_test($this->A, $this->D);
        list($BT,$B,$C,$EOB1,$D,$A,$EOB2,$E) = $this->validate_page_order(array($this->bt, $this->B, $this->C,
                                                                                $this->eob_1, $this->D, $this->A,
                                                                                $this->eob_2, $this->E));
        $this->validate_page_pointers($BT, $this->B, null, 'BT');
        $this->validate_page_pointers($B, $this->C, $this->branch_1, 'B');
        $this->validate_page_pointers($C, $this->eob_1, $this->branch_1, 'C');
        $this->validate_page_pointers($EOB1, $this->bt, $this->branch_1, 'EOB1');
        $this->validate_page_pointers($D, $this->A, $this->branch_2, 'D');
        $this->validate_page_pointers($A, $this->eob_2, $this->branch_2, 'A');
        $this->validate_page_pointers($EOB2, $this->E, $this->branch_2, 'EOB2');
        $this->validate_page_pointers($E, 0, null, 'E');

        $this->validate_branch_firstpages($this->bt, array($this->B, $this->D));
    }
    // A before E
    public function test_a_before_e() {
        $this->move_test($this->A, $this->eob_2);
        list($BT,$B,$C,$EOB1,$D,$EOB2,$A,$E) = $this->validate_page_order(array($this->bt, $this->B, $this->C,
                                                                                $this->eob_1, $this->D, $this->eob_2,
                                                                                $this->A, $this->E));
        $this->validate_page_pointers($BT, $this->B, null, 'BT');
        $this->validate_page_pointers($B, $this->C, $this->branch_1, 'B');
        $this->validate_page_pointers($C, $this->eob_1, $this->branch_1, 'C');
        $this->validate_page_pointers($EOB1, $this->bt, $this->branch_1, 'EOB1');
        $this->validate_page_pointers($D, $this->eob_2, $this->branch_2, 'D');
        $this->validate_page_pointers($EOB2, $this->A, $this->branch_2, 'EOB2');
        $this->validate_page_pointers($A, $this->E, null, 'A');
        $this->validate_page_pointers($E, 0, null, 'E');

        $this->validate_branch_firstpages($this->bt, array($this->B, $this->D));
    }
    // A after E
    public function test_a_after_e() {
        $this->move_test($this->A, $this->E);
        list($BT,$B,$C,$EOB1,$D,$EOB2,$E,$A) = $this->validate_page_order(array($this->bt, $this->B, $this->C,
                                                                                $this->eob_1, $this->D, $this->eob_2,
                                                                                $this->E, $this->A));
        $this->validate_page_pointers($BT, $this->B, null, 'BT');
        $this->validate_page_pointers($B, $this->C, $this->branch_1, 'B');
        $this->validate_page_pointers($C, $this->eob_1, $this->branch_1, 'C');
        $this->validate_page_pointers($EOB1, $this->bt, $this->branch_1, 'EOB1');
        $this->validate_page_pointers($D, $this->eob_2, $this->branch_2, 'D');
        $this->validate_page_pointers($EOB2, $this->E, $this->branch_2, 'EOB2');
        $this->validate_page_pointers($E, $this->A, null, 'E');
        $this->validate_page_pointers($A, 0, null, 'A');

        $this->validate_branch_firstpages($this->bt, array($this->B, $this->D));
    }
}


class languagelesson_move_test_complex_eobs extends languagelesson_move_test {

    private $A = 570;
    private $bt = 571;
    private $B = 574;
    private $C = 575;
    private $eob_1 = 572;
    private $D = 576;
    private $E = 577;
    private $eob_2 = 573;
    private $F = 578;

    private $branch_1 = 180;
    private $branch_2 = 181;

    function __construct() {
        $this->TESTLLID = 49;
    }

    public function setUp() {
        $pages = array(
                    array ( 'id' => $this->A,
                            'branchid' => null,
                            'prevpageid' => 0,
                            'nextpageid' => $this->bt,
                            'ordering' => 1),
                    array ( 'id' => $this->bt,
                            'branchid' => null,
                            'prevpageid' => $this->A,
                            'nextpageid' => $this->B,
                            'ordering' => 2),
                    array ( 'id' => $this->B,
                            'branchid' => $this->branch_1,
                            'prevpageid' => $this->bt,
                            'nextpageid' => $this->C,
                            'ordering' => 3),
                    array ( 'id' => $this->C,
                            'branchid' => $this->branch_1,
                            'prevpageid' => $this->B,
                            'nextpageid' => $this->eob_1,
                            'ordering' => 4),
                    array ( 'id' => $this->eob_1,
                            'branchid' => $this->branch_1,
                            'prevpageid' => $this->C,
                            'nextpageid' => $this->bt,
                            'ordering' => 5),
                    array ( 'id' => $this->D,
                            'branchid' => $this->branch_2,
                            'prevpageid' => $this->eob_1,
                            'nextpageid' => $this->E,
                            'ordering' => 6),
                    array ( 'id' => $this->E,
                            'branchid' => $this->branch_2,
                            'prevpageid' => $this->D,
                            'nextpageid' => $this->eob_2,
                            'ordering' => 7),
                    array ( 'id' => $this->eob_2,
                            'branchid' => $this->branch_2,
                            'prevpageid' => $this->E,
                            'nextpageid' => $this->F,
                            'ordering' => 8),
                    array ( 'id' => $this->F,
                            'branchid' => null,
                            'prevpageid' => $this->eob_2,
                            'nextpageid' => 0,
                            'ordering' => 9)
                );

        $branches = array(
                        array ( 'id' => $this->branch_1,
                                'firstpage' => $this->B,
                                'ordering' => 1),
                        array ( 'id' => $this->branch_2,
                                'firstpage' => $this->D,
                                'ordering' => 2)
                );

        $this->genericSetUp($pages, $branches);
    }

    public function tearDown() {
        // reset BOTH of the EOB pages
        $this->move_test($this->eob_1, $this->C);
        $this->move_test($this->eob_2, $this->E);
        // validate
        list($A,$BT,$B,$C,$EOB1,$D,$E,$EOB2,$F) = $this->validate_page_order(array($this->A, $this->bt, $this->B,
                                                                                   $this->C, $this->eob_1, $this->D,
                                                                                   $this->E, $this->eob_2, $this->F));
        $this->validate_page_pointers($A, $this->bt, null, 'A');
        $this->validate_page_pointers($BT, $this->B, null, 'BT');
        $this->validate_page_pointers($B, $this->C, $this->branch_1, 'B');
        $this->validate_page_pointers($C, $this->eob_1, $this->branch_1, 'C');
        $this->validate_page_pointers($EOB1, $this->bt, $this->branch_1, 'EOB1');
        $this->validate_page_pointers($D, $this->E, $this->branch_2, 'D');
        $this->validate_page_pointers($E, $this->eob_2, $this->branch_2, 'E');
        $this->validate_page_pointers($EOB2, $this->F, $this->branch_2, 'EOB2');
        $this->validate_page_pointers($F, 0, null, 'F');

        $this->validate_branch_firstpages($this->bt, array($this->B, $this->D));
    }

    // move EOB1 before B
    public function test_all_pages_in_branch2() {
        $this->move_test($this->eob_1, $this->bt);
        list($A,$BT,$EOB1,$B,$C,$D,$E,$EOB2,$F) = $this->validate_page_order(array($this->A, $this->bt, $this->eob_1,
                                                                                   $this->B, $this->C, $this->D,
                                                                                   $this->E, $this->eob_2, $this->F));

        $this->validate_page_pointers($A, $this->bt, null, 'A');
        $this->validate_page_pointers($BT, $this->eob_1, null, 'BT');
        $this->validate_page_pointers($EOB1, $this->bt, $this->branch_1, 'EOB1');
        $this->validate_page_pointers($B, $this->C, $this->branch_2, 'B');
        $this->validate_page_pointers($C, $this->D, $this->branch_2, 'C');
        $this->validate_page_pointers($D, $this->E, $this->branch_2, 'D');
        $this->validate_page_pointers($E, $this->eob_2, $this->branch_2, 'E');
        $this->validate_page_pointers($EOB2, $this->F, $this->branch_2, 'EOB2');
        $this->validate_page_pointers($F, 0, null, 'F');

        $this->validate_branch_firstpages($this->bt, array(0, $this->B));
    }
    // move EOB1 before C
    public function test_branch2_subsume_c() {
        $this->move_test($this->eob_1, $this->B);
        list($A,$BT,$B,$EOB1,$C,$D,$E,$EOB2,$F) = $this->validate_page_order(array($this->A, $this->bt, $this->B,
                                                                                   $this->eob_1, $this->C, $this->D,
                                                                                   $this->E, $this->eob_2, $this->F));

        $this->validate_page_pointers($A, $this->bt, null, 'A');
        $this->validate_page_pointers($BT, $this->B, null, 'BT');
        $this->validate_page_pointers($B, $this->eob_1, $this->branch_1, 'B');
        $this->validate_page_pointers($EOB1, $this->bt, $this->branch_1, 'EOB1');
        $this->validate_page_pointers($C, $this->D, $this->branch_2, 'C');
        $this->validate_page_pointers($D, $this->E, $this->branch_2, 'D');
        $this->validate_page_pointers($E, $this->eob_2, $this->branch_2, 'E');
        $this->validate_page_pointers($EOB2, $this->F, $this->branch_2, 'EOB2');
        $this->validate_page_pointers($F, 0, null, 'F');

        $this->validate_branch_firstpages($this->bt, array($this->B, $this->C));
    }
    // move EOB1 after D
    public function test_branch1_subsume_d() {
        $this->move_test($this->eob_1, $this->D);
        list($A,$BT,$B,$C,$D,$EOB1,$E,$EOB2,$F) = $this->validate_page_order(array($this->A, $this->bt, $this->B,
                                                                                   $this->C, $this->D, $this->eob_1,
                                                                                   $this->E, $this->eob_2, $this->F));

        $this->validate_page_pointers($A, $this->bt, null, 'A');
        $this->validate_page_pointers($BT, $this->B, null, 'BT');
        $this->validate_page_pointers($B, $this->C, $this->branch_1, 'B');
        $this->validate_page_pointers($C, $this->D, $this->branch_1, 'C');
        $this->validate_page_pointers($D, $this->eob_1, $this->branch_1, 'D');
        $this->validate_page_pointers($EOB1, $this->bt, $this->branch_1, 'EOB1');
        $this->validate_page_pointers($E, $this->eob_2, $this->branch_2, 'E');
        $this->validate_page_pointers($EOB2, $this->F, $this->branch_2, 'EOB2');
        $this->validate_page_pointers($F, 0, null, 'F');

        $this->validate_branch_firstpages($this->bt, array($this->B, $this->E));
    }
    // move EOB1 after E
    public function test_all_pages_in_branch1() {
        $this->move_test($this->eob_1, $this->E);
        list($A,$BT,$B,$C,$D,$E,$EOB1,$EOB2,$F) = $this->validate_page_order(array($this->A, $this->bt, $this->B,
                                                                                   $this->C, $this->D, $this->E,
                                                                                   $this->eob_1, $this->eob_2, $this->F));

        $this->validate_page_pointers($A, $this->bt, null, 'A');
        $this->validate_page_pointers($BT, $this->B, null, 'BT');
        $this->validate_page_pointers($B, $this->C, $this->branch_1, 'B');
        $this->validate_page_pointers($C, $this->D, $this->branch_1, 'C');
        $this->validate_page_pointers($D, $this->E, $this->branch_1, 'D');
        $this->validate_page_pointers($E, $this->eob_1, $this->branch_1, 'E');
        $this->validate_page_pointers($EOB1, $this->bt, $this->branch_1, 'EOB1');
        $this->validate_page_pointers($EOB2, $this->F, $this->branch_2, 'EOB2');
        $this->validate_page_pointers($F, 0, null, 'F');

        $this->validate_branch_firstpages($this->bt, array($this->B, 0));
    }
    // move EOB2 before D
    public function test_branch2_eject_d_and_e() {
        $this->move_test($this->eob_2, $this->eob_1);
        list($A,$BT,$B,$C,$EOB1,$EOB2,$D,$E,$F) = $this->validate_page_order(array($this->A, $this->bt, $this->B,
                                                                                   $this->C, $this->eob_1, $this->eob_2,
                                                                                   $this->D, $this->E, $this->F));

        $this->validate_page_pointers($A, $this->bt, null, 'A');
        $this->validate_page_pointers($BT, $this->B, null, 'BT');
        $this->validate_page_pointers($B, $this->C, $this->branch_1, 'B');
        $this->validate_page_pointers($C, $this->eob_1, $this->branch_1, 'C');
        $this->validate_page_pointers($EOB1, $this->bt, $this->branch_1, 'EOB1');
        $this->validate_page_pointers($EOB2, $this->D, $this->branch_2, 'EOB2');
        $this->validate_page_pointers($D, $this->E, null, 'D');
        $this->validate_page_pointers($E, $this->F, null, 'E');
        $this->validate_page_pointers($F, 0, null, 'F');

        $this->validate_branch_firstpages($this->bt, array($this->B, 0));
    }
    // move EOB2 before E
    public function test_branch2_eject_e() {
        $this->move_test($this->eob_2, $this->D);
        list($A,$BT,$B,$C,$EOB1,$D,$EOB2,$E,$F) = $this->validate_page_order(array($this->A, $this->bt, $this->B,
                                                                                   $this->C, $this->eob_1, $this->D,
                                                                                   $this->eob_2, $this->E, $this->F));

        $this->validate_page_pointers($A, $this->bt, null, 'A');
        $this->validate_page_pointers($BT, $this->B, null, 'BT');
        $this->validate_page_pointers($B, $this->C, $this->branch_1, 'B');
        $this->validate_page_pointers($C, $this->eob_1, $this->branch_1, 'C');
        $this->validate_page_pointers($EOB1, $this->bt, $this->branch_1, 'EOB1');
        $this->validate_page_pointers($D, $this->eob_2, $this->branch_2, 'D');
        $this->validate_page_pointers($EOB2, $this->E, $this->branch_2, 'EOB2');
        $this->validate_page_pointers($E, $this->F, null, 'E');
        $this->validate_page_pointers($F, 0, null, 'F');

        $this->validate_branch_firstpages($this->bt, array($this->B, $this->D));
    }
    // move EOB2 after F
    public function test_branch2_subsume_f() {
        $this->move_test($this->eob_2, $this->F);
        list($A,$BT,$B,$C,$EOB1,$D,$E,$F,$EOB2) = $this->validate_page_order(array($this->A, $this->bt, $this->B,
                                                                                   $this->C, $this->eob_1, $this->D,
                                                                                   $this->E, $this->F, $this->eob_2));

        $this->validate_page_pointers($A, $this->bt, null, 'A');
        $this->validate_page_pointers($BT, $this->B, null, 'BT');
        $this->validate_page_pointers($B, $this->C, $this->branch_1, 'B');
        $this->validate_page_pointers($C, $this->eob_1, $this->branch_1, 'C');
        $this->validate_page_pointers($EOB1, $this->bt, $this->branch_1, 'EOB1');
        $this->validate_page_pointers($D, $this->E, $this->branch_2, 'D');
        $this->validate_page_pointers($E, $this->F, $this->branch_2, 'E');
        $this->validate_page_pointers($F, $this->eob_2, $this->branch_2, 'F');
        $this->validate_page_pointers($EOB2, 0, $this->branch_2, 'EOB2');

        $this->validate_branch_firstpages($this->bt, array($this->B, $this->D));
    }
    
}
?>
