<?php

// pull in config
require_once(dirname(__FILE__) . "/../../../../config.php");
global $CFG;

// pull in insertion code (convenient for auto-populating for tests AFTER testing insertion)

abstract class languagelesson_page_actions_test extends UnitTestCase {

    protected $TESTLLID;

    /**
     * Make sure that the pages in the LL are in the order specified
     * @param array(int) $desired_order The list of pageID values in the order expected
     * @return array(object) The page records of the LL, in order
     */
    protected function validate_page_order($desired_order) {
        $pages = get_records('languagelesson_pages', 'lessonid', $this->TESTLLID, 'ordering');
        $actual_order = array_keys($pages);

        for ($i=0; $i<count($desired_order); $i++) {
            $this->assertEqual($actual_order[$i], $desired_order[$i], 'page '.($i+1).' ID not in place');
        }

        // store the pages in order without their ID keys
        $return_pages = array();
        // add on markers for the beginning and end of the LL so we can do paired checking of pointers
        $check_order = array_merge(array(0), $desired_order, array(0));
        for ($i=1; $i<count($check_order)-1; $i++) {
            $page = $pages[$check_order[$i]];
            $this->assertEqual($page->prevpageid, $check_order[$i-1], "page $i has incorrect prevpageid");
            $this->assertEqual($page->ordering, $i, "page $i has incorrect ordering");
            $return_pages[] = $page;
        }

        return $return_pages;
    }

    protected function validate_page_pointers($page, $nextpageid, $branchid, $pgstr) {
        $this->assertEqual($page->nextpageid, $nextpageid, "Page {$pgstr}'s nextpageid is wrong");
        $this->assertEqual($page->branchid, $branchid, "Page {$pgstr}'s branchid is wrong"); 
    }

    protected function validate_branch_firstpages($btid, $desired_firstpages) {
        if (!$branches = get_records('languagelesson_branches', 'parentid', $btid, 'ordering')) {
            $branches = array();
        }
        $sequential_branches = array_values($branches);

        $this->assertEqual(count($desired_firstpages), count($sequential_branches), "number of branches is incorrect");
        for ($i=0; $i<count($desired_firstpages); $i++) {
            $branch = $sequential_branches[$i];
            $this->assertEqual($branch->firstpage, $desired_firstpages[$i], "branch $i has incorrect firstpageid");
        }
    }

    protected function get_bt_spawns($btid) {
        $branch_ids = array_keys(get_records('languagelesson_branches', 'parentid', $btid, 'ordering'));
        $eob_ids = array_keys(get_records_select('languagelesson_pages', 'branchid in ('.implode(',',$branch_ids).')
                                                                          AND qtype='.LL_ENDOFBRANCH, 'ordering'));
        return array($eob_ids, $branch_ids);
    }


    // insertion functions
    protected function insert_question_page($prevpageid, $title='foo') {
        $page = new stdClass;
        $page->qtype = LL_TRUEFALSE;
        $page->maxanswers = 2;
        $page->title = $title;
        $page->contents = 't';

        $page->pageid = $prevpageid;

        $page->answer = array( 't', 'f' );
        $page->jumpto = array( LL_NEXTPAGE, 0 );
        $page->score  = array( 1, 0 );
        
        return $this->create_page($page);
    }

    protected function insert_branch_table($prevpageid, $branches, $title='foo') {
        $page = new stdClass;
        $page->qtype = LL_BRANCHTABLE;
        $page->maxanswers = count($branches);
        $page->title = $title; 
        $page->contents = 'foo';

        $page->pageid = $prevpageid;

        $page->answer = array_map(function($foo) { return 'foo'; }, $branches);
        $page->jumpto = $branches;

        return $this->create_page($page);
    }

    private function create_page($page) {
        $testll = get_record('languagelesson', 'id', $this->TESTLLID);
        $inserter = new languagelesson_page_inserter();
        $page = $inserter->insert($testll, $page, time());
        return $page->id;
    }
}

?>
