<?php

// pull in config
require_once(dirname(__FILE__) . "/../../../../../config.php");
global $CFG;
// pull in the base test class
require_once($CFG->dirroot.'/mod/languagelesson/simpletest/action/insert/base_test_class.php');
/*
 *   -> qtype                   :: The question type of the page to insert
 *   -> maxanswers
 *   -> title
 *   -> contents
 *   -> layout
 *   -> pageid
 *
 *   -> answer
 *   -> response
 *   -> jumpto
 *   -> score
 *   -> dropdown 
 *
 *   -> correctresponse
 *   -> correctresponsescore
 *   -> correctanswerjump
 *   -> wrongresponse
 *   -> wrongresponsescore
 *   -> wronganswerjump
 */


class languagelesson_insert_test_complex extends languagelesson_insertion_test_base {

    public function __construct() {
        $this->TESTLLID = 50;
    }

    /******************************/
    // TEST CASES
    /******************************/

    // put a BT in an empty LL
    public function test_bt_in_empty_ll() {
        $bt = $this->insert_branch_table(null, array(0,0));
        list($eob_ids, $branch_ids) = $this->get_bt_spawns($bt);

        list($BT, $EOB1, $EOB2) = $this->validate_page_order(array($bt, $eob_ids[0], $eob_ids[1]));
        $this->validate_page_pointers($BT, $EOB1->id, null, 'BT');
        $this->validate_page_pointers($EOB1, $BT->id, $branch_ids[0], 'EOB1');
        $this->validate_page_pointers($EOB2, 0, $branch_ids[1], 'EOB2');
    }

    // put in a qpage and a BT after it
    public function test_bt_after_existing_qpage() {
        $firstpage = $this->insert_question_page(null);
        $bt = $this->insert_branch_table($firstpage, array(0,0));
        list($eob_ids, $branch_ids) = $this->get_bt_spawns($bt);

        list($A, $BT, $EOB1, $EOB2) = $this->validate_page_order(array($firstpage, $bt, $eob_ids[0], $eob_ids[1]));
        $this->validate_page_pointers($A, $BT->id, null, 'A');
        $this->validate_page_pointers($BT, $EOB1->id, null, 'BT');
        $this->validate_page_pointers($EOB2, 0, $branch_ids[1], 'EOB2');
    }

    // put in a BT and another BT after it
    public function test_bt_after_existing_bt() {
        $firstbt = $this->insert_branch_table(null, array(0,0));
        list($first_eob_ids, $first_branch_ids) = $this->get_bt_spawns($firstbt);

        $secondbt = $this->insert_branch_table($first_eob_ids[1], array(0,0));
        list($second_eob_ids, $second_branch_ids) = $this->get_bt_spawns($secondbt);

        list($bt1, $eob1, $eob2, $bt2, $eob3, $eob4) = $this->validate_page_order(array($firstbt, $first_eob_ids[0],
                                                                                        $first_eob_ids[1], $secondbt,
                                                                                        $second_eob_ids[0], $second_eob_ids[1]));
        $this->validate_page_pointers($eob2, $bt2->id, $first_branch_ids[1], 'EOB2');
        $this->validate_page_pointers($bt2, $eob3->id, null, 'BT2');
        $this->validate_page_pointers($eob4, 0, $second_branch_ids[1], 'EOB4');
    }

    // put in a qpage, another after it, then a BT between them
    public function test_bt_between_existing_pages_qpage() {
        $firstpage = $this->insert_question_page(null);
        $lastpage = $this->insert_question_page($firstpage);
        $bt = $this->insert_branch_table($firstpage, array($lastpage,0));
        list($eob_ids, $branch_ids) = $this->get_bt_spawns($bt);

        list($A,$BT,$B,$EOB1,$EOB2) = $this->validate_page_order(array($firstpage, $bt, $lastpage, $eob_ids[0], $eob_ids[1]));
        $this->validate_page_pointers($A, $BT->id, null, 'A');
        $this->validate_page_pointers($BT, $B->id, null, 'BT');
        $this->validate_page_pointers($B, $EOB1->id, $branch_ids[0], 'B');
        $this->validate_page_pointers($EOB2, 0, $branch_ids[1], 'EOB2');

        $this->validate_branch_firstpages($BT->id, array($lastpage, 0));
    }

    // put in a BT, a qpage after it, and a BT between them
    public function test_bt_between_existing_pages_bt() {
        $firstbt = $this->insert_branch_table(null, array(0,0));
        list($first_eob_ids, $first_branch_ids) = $this->get_bt_spawns($firstbt);
        $lastpage = $this->insert_question_page($first_eob_ids[1]);
        $secondbt = $this->insert_branch_table($first_eob_ids[1], array($lastpage,0));
        list($second_eob_ids, $second_branch_ids) = $this->get_bt_spawns($secondbt);

        list($BT1,$EOB1,$EOB2,$BT2,$A,$EOB3,$EOB4) = $this->validate_page_order(array($firstbt, $first_eob_ids[0],
                                                                                      $first_eob_ids[1], $secondbt,
                                                                                      $lastpage, $second_eob_ids[0],
                                                                                      $second_eob_ids[1]));
        $this->validate_page_pointers($EOB2, $BT2->id, $first_branch_ids[1], 'EOB2');
        $this->validate_page_pointers($BT2, $A->id, null, 'BT2');
        $this->validate_page_pointers($A, $EOB3->id, $second_branch_ids[0], 'A');
        $this->validate_page_pointers($EOB4, 0, $second_branch_ids[1], 'EOB4');

        $this->validate_branch_firstpages($BT1->id, array(0,0));
        $this->validate_branch_firstpages($BT2->id, array($lastpage, 0));
    }

    // put in a BT, then a qpage in the first branch
    public function test_qpage_first_in_first_branch() {
        $bt = $this->insert_branch_table(null, array(0,0));
        list($eob_ids, $branch_ids) = $this->get_bt_spawns($bt);
        $qpage = $this->insert_question_page($bt);

        list($BT,$A,$EOB1,$EOB2) = $this->validate_page_order(array($bt,$qpage,$eob_ids[0],$eob_ids[1]));
        $this->validate_page_pointers($BT, $A->id, null, 'BT');
        $this->validate_page_pointers($A, $EOB1->id, $branch_ids[0], 'A');
        $this->validate_page_pointers($EOB1, $BT->id, $branch_ids[0], 'EOB1');
        $this->validate_page_pointers($EOB2, 0, $branch_ids[1], 'EOB2');

        $this->validate_branch_firstpages($bt, array($qpage, 0));
    }

    // put in a BT, then a qpage in the second branch
    public function test_qpage_first_in_second_branch() {
        $bt = $this->insert_branch_table(null, array(0,0));
        list($eob_ids, $branch_ids) = $this->get_bt_spawns($bt);
        $qpage = $this->insert_question_page($eob_ids[0]);

        list($BT,$EOB1,$A,$EOB2) = $this->validate_page_order(array($bt,$eob_ids[0],$qpage,$eob_ids[1]));
        $this->validate_page_pointers($BT, $EOB1->id, null, 'BT');
        $this->validate_page_pointers($EOB1, $BT->id, $branch_ids[0], 'EOB1');
        $this->validate_page_pointers($A, $EOB2->id, $branch_ids[1], 'A');
        $this->validate_page_pointers($EOB2, 0, $branch_ids[1], 'EOB2');

        $this->validate_branch_firstpages($bt, array(0, $qpage));
    }

    // put in a BT, then three pages in the first branch
    public function test_qpage_in_populated_branch() {
        $bt = $this->insert_branch_table(null, array(0,0));
        list($eob_ids, $branch_ids) = $this->get_bt_spawns($bt);
        $qpage1 = $this->insert_question_page($bt);
        $qpage2 = $this->insert_question_page($qpage1);

        list($BT,$A,$B,$EOB1,$EOB2) = $this->validate_page_order(array($bt,$qpage1,$qpage2,$eob_ids[0],$eob_ids[1]));
        $this->validate_page_pointers($BT, $A->id, null, 'BT');
        $this->validate_page_pointers($A, $B->id, $branch_ids[0], 'A');
        $this->validate_page_pointers($B, $EOB1->id, $branch_ids[0], 'B');

        $this->validate_branch_firstpages($bt, array($qpage1,0));

        $qpage3 = $this->insert_question_page($qpage1);
        list($BT,$A,$C,$B,$EOB1,$EOB2) = $this->validate_page_order(array($bt,$qpage1,$qpage3,$qpage2,$eob_ids[0],$eob_ids[1]));
        $this->validate_page_pointers($A, $C->id, $branch_ids[0], 'A');
        $this->validate_page_pointers($C, $B->id, $branch_ids[0], 'C');

        $this->validate_branch_firstpages($bt, array($qpage1,0));
    }

    // put in 3 qpages, then put in a BT framing the last two
    public function test_subsume_qpages() {
        $qpage1 = $this->insert_question_page(null);
        $qpage2 = $this->insert_question_page($qpage1);
        $qpage3 = $this->insert_question_page($qpage2);
        $bt = $this->insert_branch_table($qpage1, array($qpage2, $qpage3));
        list($eob_ids, $branch_ids) = $this->get_bt_spawns($bt);

        list($A,$BT,$B,$EOB1,$C,$EOB2) = $this->validate_page_order(array($qpage1,$bt,$qpage2,$eob_ids[0],$qpage3,$eob_ids[1]));
        $this->validate_page_pointers($BT, $B->id, null, 'BT');
        $this->validate_page_pointers($B, $EOB1->id, $branch_ids[0], 'B');
        $this->validate_page_pointers($EOB1, $BT->id, $branch_ids[0], 'EOB1');
        $this->validate_page_pointers($C, $EOB2->id, $branch_ids[1], 'C');
        $this->validate_page_pointers($EOB2, 0, $branch_ids[1], 'EOB2');

        $this->validate_branch_firstpages($bt, array($qpage2, $qpage3));
    }

    // put in a qpage, a BT, two qpages inside the BT, another BT framing the second of those (nested), and another qpage
    // after the nested BT
    public function test_subsume_bt() {
        $qpage1 = $this->insert_question_page(null);
        $bt1 = $this->insert_branch_table($qpage1, array(0,0));
        list($eob_ids1, $branch_ids1) = $this->get_bt_spawns($bt1);
        $qpage2 = $this->insert_question_page($bt1);
        $qpage3 = $this->insert_question_page($qpage2);
        $bt2 = $this->insert_branch_table($qpage2, array($qpage3, 0));
        list($eob_ids2, $branch_ids2) = $this->get_bt_spawns($bt2);
        $qpage4 = $this->insert_question_page($eob_ids2[1]);

        list($A,$BT1,$B,$BT2,$C,$EOB3,$EOB4,$D,$EOB1,$EOB2) = $this->validate_page_order(array($qpage1,$bt1,$qpage2,$bt2,
                                                                                               $qpage3,$eob_ids2[0],$eob_ids2[1],
                                                                                               $qpage4,$eob_ids1[0],
                                                                                               $eob_ids1[1]));
        $this->validate_page_pointers($A, $BT1->id, null, 'A');
        $this->validate_page_pointers($BT1, $B->id, null, 'BT1');
        $this->validate_page_pointers($B, $BT2->id, $branch_ids1[0], 'B');
        $this->validate_page_pointers($BT2, $C->id, $branch_ids1[0], 'BT2');
        $this->validate_page_pointers($C, $EOB3->id, $branch_ids2[0], 'C');
        $this->validate_page_pointers($EOB3, $BT2->id, $branch_ids2[0], 'EOB3');
        $this->validate_page_pointers($EOB4, $D->id, $branch_ids2[1], 'EOB4');
        $this->validate_page_pointers($D, $EOB1->id, $branch_ids1[0], 'D');
        $this->validate_page_pointers($EOB1, $BT1->id, $branch_ids1[0], 'EOB1');
        $this->validate_page_pointers($EOB2, 0, $branch_ids1[1], 'EOB2');
    }

}

?>
