<?php

// pull in config
require_once(dirname(__FILE__) . "/../../../../../config.php");
global $CFG;
// pull in the base test class
require_once($CFG->dirroot.'/mod/languagelesson/simpletest/action/insert/base_test_class.php');
/*
 *   -> qtype                   :: The question type of the page to insert
 *   -> maxanswers
 *   -> title
 *   -> contents
 *   -> layout
 *   -> pageid
 *
 *   -> answer
 *   -> response
 *   -> jumpto
 *   -> score
 *   -> dropdown 
 *
 *   -> correctresponse
 *   -> correctresponsescore
 *   -> correctanswerjump
 *   -> wrongresponse
 *   -> wrongresponsescore
 *   -> wronganswerjump
 */


class languagelesson_insert_test_simple extends languagelesson_insertion_test_base {

    public function __construct() {
        $this->TESTLLID = 50;
    }

    /******************************/
    // TEST CASES
    /******************************/

    public function test_insert_single_page() {
        $pageid = $this->insert_question_page(null);

        list($A) = $this->validate_page_order(array($pageid));
        $this->validate_page_pointers($A, 0, null, 'A');
    }

    public function test_insert_new_last_page() {
        $lastpageid = $this->insert_question_page(0);
        $newlastpageid = $this->insert_question_page($lastpageid);

        list($A,$B) = $this->validate_page_order(array($lastpageid, $newlastpageid));
        $this->validate_page_pointers($A, $newlastpageid, null, 'A');
        $this->validate_page_pointers($B, 0, null, 'B');
    }

    public function test_insert_page_between_existing() {
        $firstpageid = $this->insert_question_page(0);
        $lastpageid = $this->insert_question_page($firstpageid);
        $middlepageid = $this->insert_question_page($firstpageid);

        list($A,$B,$C) = $this->validate_page_order(array($firstpageid, $middlepageid, $lastpageid));
        $this->validate_page_pointers($A, $middlepageid, null, 'A');
        $this->validate_page_pointers($B, $lastpageid, null, 'B');
        $this->validate_page_pointers($C, 0, null, 'A');
    }

}

?>
