<?php

// pull in config
require_once(dirname(__file__) . "/../../../../../config.php");
global $CFG;
// pull in the base test class
require_once($CFG->dirroot.'/mod/languagelesson/simpletest/action/base_action_test_class.php');

abstract class languagelesson_insertion_test_base extends languagelesson_page_actions_test {

    public function setUp() { 
        // TODO: use (tested!) page_delete functionality to do this
        delete_records('languagelesson_pages', 'lessonid', $this->TESTLLID);
        delete_records('languagelesson_answers', 'lessonid', $this->TESTLLID);
        delete_records('languagelesson_branches', 'lessonid', $this->TESTLLID);
    }

    public function tearDown() { 
    }

}

?>
