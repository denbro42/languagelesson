<?php

// pull in config
require_once(dirname(__FILE__) . "/../../../config.php");
global $CFG;

class test_everything extends TestSuite {

    function __construct() {
        global $CFG;
        $this->TestSuite('All tests');
        // insert
        $this->addTestFile($CFG->dirroot.'/mod/languagelesson/simpletest/action/insert/test_insert_simple.php');
        $this->addTestFile($CFG->dirroot.'/mod/languagelesson/simpletest/action/insert/test_insert_complex.php');
        // delete
        $this->addTestFile($CFG->dirroot.'/mod/languagelesson/simpletest/action/delete/test_delete_simple.php');
        $this->addTestFile($CFG->dirroot.'/mod/languagelesson/simpletest/action/delete/test_delete_complex.php');
        // move
        $this->addTestFile($CFG->dirroot.'/mod/languagelesson/simpletest/action/move/test_move_simple.php');
        $this->addTestFile($CFG->dirroot.'/mod/languagelesson/simpletest/action/move/test_move_complex.php');
        // update
    }

}

?>
