<?php // $Id: view.php 677 2011-10-12 18:38:45Z griffisd $
/**
 * This page prints the main interface for a languagelesson instance
 *
 * @package languagelesson
 * @category mod 
 * @version $Id: view.php 677 2011-10-12 18:38:45Z griffisd $
 * @author $Author: griffisd $
 * @license http://www.gnu.org/copyleft/gpl.html GNU Public License
 */

require_once('../../config.php');

require_once($CFG->dirroot.'/mod/languagelesson/locallib.php');
require_once($CFG->dirroot.'/mod/languagelesson/lib.php');
require_once($CFG->dirroot.'/mod/languagelesson/pagelib.php');

require_once($CFG->libdir.'/blocklib.php');
require_once($CFG->libdir.'/accesslib.php');

$id      = required_param('id', PARAM_INT);             // Course Module ID
$pageid  = optional_param('pageid', NULL, PARAM_INT);   // Lesson Page ID
$edit    = optional_param('edit', -1, PARAM_BOOL);      // marks if user is editing page blocks

list($cm, $course, $lesson) = languagelesson_get_basics($id);

require_login($course->id, false, $cm);

// check if the user is trying to view the contents of an empty branch
if ($branchnocontent = optional_param('branchnocontent', 0, PARAM_INT)) {
    error('You are attempting to view a branch that has no pages!');
}

// instantiate the viewer and make sure this LL has pages to display;
// if it does not, viewer will be redirected based on their role
$viewer = new languagelesson_page_viewer($cm, $course, $lesson);
$viewer->verify_has_pages();

// make sure that this lesson is available at all
$viewer->check_ll_availability();

switch ($pageid) {

    case LL_EOL:
        $viewer->display_eol();
        break;

    case null:
        if ($viewer->has_old_data()) {
            $viewer->display_landing_page();
            break;

        } else {
            $pageid = $viewer->startLL();
        }
    default:
        if (!$page = get_record('languagelesson_pages', 'id', $pageid)) {
            error('Language Lesson has pages, but unable to fetch the desired page!');
        }

        $viewer->set_page($page);

        // check if reviewing
        $reviewing = optional_param('reviewing', 0, PARAM_INT);
        if ($reviewing) { $viewer->flag_reviewing(); }

        // check if they submitted an answer
        $noanswer = optional_param('noanswer', 0, PARAM_INT);

        // mark if we should be showing feedback (default no)
        $showfeedback = optional_param('showfeedback', 0, PARAM_INT);
        // pull any attempt data that continue.php handed back
        $submittedattempt = new stdClass;
        $submittedattempt->id = optional_param('aid', 0, PARAM_INT);
        $submittedattempt->text = optional_param('atext', '', PARAM_RAW);
        // pull the nextpageid that continue.php handed back
        $savednextpageid = optional_param('nextpageid', 0, PARAM_INT);


        // and actually view the page
        $viewer->display_qpage($noanswer, $showfeedback, $submittedattempt, $savednextpageid, $edit);

        break;
}





class languagelesson_page_viewer {


    // basic information about where we are
    private $cm = null;
    private $cmid = 0;
    private $course = null;
    private $lesson = null;
    private $page = null;

    // for convenience, store if student or teacher is viewing
    private $isstudent = true;
    // is the student looking at a completed, non-updatable test?
    private $reviewing = false;
    // should we be displaying their old attempt information?
    private $showoldattempt = false;

    // structural variables for the displayed page
    private $PAGE = null;
    private $pageblocks = null;
    private $leftcolumnwidth = 0;
    private $rightcolumnwidth = 0;
    private $timer = null;
    


    function __construct($cm, $course, $lesson) {
        $this->cm = $cm;
        $this->cmid = $cm->id;
        $this->course = $course;
        $this->lesson = $lesson;

        // check if student or teacher
        $context = get_context_instance(CONTEXT_MODULE, $cm->id);
        $this->isstudent = (!has_capability('mod/languagelesson:manage', $context));
    }
        

    /**
     * Set method for reviewing bool flag
     */
    public function flag_reviewing() {
        $this->reviewing = true;
    }
        

    /**
     * Set method for page value
     */
    public function set_page($page) {
        $this->page = $page;
    }


    /**
     * Checks if there are any LL pages; if none, print a warning to students and give teachers
     * the lesson creation interface
     * @uses $CFG
     */
    public function verify_has_pages() {
        global $CFG;
        if (!get_field('languagelesson_pages', 'id', 'lessonid', $this->lesson->id, 'prevpageid', 0)) {
            // if it's a student, print a warning message to them
            if ($this->isstudent) {
                languagelesson_set_message(get_string('lessonnotready', 'languagelesson', $this->course->teacher));
            // it it's a teacher, give them a creation interface
            } else {
                if (!count_records('languagelesson_pages', 'lessonid', $this->lesson->id)) {
                    redirect("$CFG->wwwroot/mod/languagelesson/edit.php?id=$this->cmid"); // no pages - redirect to add pages
                } else {
                    languagelesson_set_message(get_string('lessonpagelinkingbroken', 'languagelesson'));  // ok, bad mojo
                }
            }
        }
    }


    /**
     * Takes one of two actions when view.php is given no pageid value:
     *   - if the student has attempted the LL before, directs them to the landing page displaying old grade
     *   - if they haven't, returns the ID of the first page to start the LL
     * @uses $USER
     * @returns bool 
     */
    public function has_old_data() {
        global $USER;

        // check to see if this user has completed the lesson before
        $hascompleted = false;
        // if no pageid given see if the lesson has been started
        if ($this->isstudent && languagelesson_is_lesson_complete($this->lesson->id, $USER->id)) {
            $hascompleted = true;
        }
        
        // whether or not they've completed it before, we'll need the list of most recent
        // attempts on the questions in this lesson, so pull it
        $attempts = languagelesson_get_most_recent_attempts($this->lesson->id, $USER->id);
        
        // display the "Your current grade is" landing page if it's a graded lesson with attempts recorded, and being viewed by a
        // student
        if ($this->lesson->type != LL_TYPE_PRACTICE
                && ($hascompleted || $attempts)
                && $this->isstudent) {
            return true;
        }

        return false;
    }


    /**
     * TODO
     * @uses $CFG
     */
    public function display_qpage($noanswer, $showfeedback, $submittedattempt, $savednextpageid, $edit) {
        global $CFG;

        // set up some general variables
        $path = $CFG->wwwroot .'/course';

        // log that they hit the view page
        add_to_log($this->course->id, 'languagelesson', 'view', 'view.php?id='. $this->cmid, $this->page->id, $this->cmid);

        $this->handle_structural_redirects();

        $this->handle_messages($noanswer);

        list($lastattemptwarning, $nomoreattempts) = $this->set_attempt_warning_flags();

        $this->print_page_head($lastattemptwarning, $nomoreattempts, $edit);

        $this->print_prompt();
    
        list($oldattempt, $img) = $this->handle_old_attempt($submittedattempt);
     
        
        // get the answers in a set order, the id order
        if ($this->page->qtype != LL_BRANCHTABLE
                && $answers = get_records("languagelesson_answers", "pageid", $this->page->id, "id")) {

            echo "<form id=\"answerform\" method =\"post\" action=\"lesson.php\" autocomplete=\"off\">";
            echo '<fieldset class="invisiblefieldset">';
            echo "<input type=\"hidden\" name=\"id\" value=\"$this->cmid\" />";
            echo '<input type="hidden" name="action" value="continue" />';
            echo '<input type="hidden" name="pageid" value="'.$this->page->id.'" />';
            echo '<input type="hidden" name="sesskey" value="'.sesskey().'" />';

            // default format text options
            $options = new stdClass;
            $options->para = false; // no <p></p>
            $options->noclean = true;
            
            // handle printing answers based on page's question type    
            switch ($this->page->qtype) {
                // auto-graded types
                case LL_SHORTANSWER :
                //case LL_NUMERICAL :
                    $this->print_answers_shortanswer($oldattempt, $img, $options);
                    break;
                case LL_CLOZE :
                    $this->print_answers_cloze($answers, $oldattempt, $img, $options);
                    break;
                case LL_TRUEFALSE :
                    $this->print_answers_truefalse($answers, $oldattempt, $img, $options);
                    break;
                case LL_MULTICHOICE :
                    $this->print_answers_multichoice($answers, $oldattempt, $img, $options);
                    break;
                case LL_MATCHING :
                    $this->print_answers_matching($answers, $oldattempt, $img, $options);
                    break;
            
                // manually-graded types
                case LL_ESSAY :
                    $this->print_answer_essay($oldattempt);
                    break;
                case LL_AUDIO :
                case LL_VIDEO :
                    $this->print_av_interface($oldattempt);
                    break;

                default: // close the tags MDL-7861
                    echo ('</table>');
                    print_simple_box_end();
                    break;
            }
        
            // close the answer form
            echo '</fieldset>';
            echo "</form>\n";
        }

        else if ($this->page->qtype == LL_BRANCHTABLE
                    && $branches = get_records('languagelesson_branches', 'parentid', $this->page->id, 'ordering')) {
            $this->print_branchtable_jumps();
        }

        else {
            $this->handle_no_answers();
        }



        // print feedback and nav buttons
        if ($this->page->qtype != LL_BRANCHTABLE) { 
            ////////////////////////////////////////
            // TODO :: do we need this?
            //         - does this mean that continue never gets called on an a/v?
            //         - can showfeedback just get forced in continue?
            // if looking at an a/v page, 
            if ($this->page->qtype == LL_AUDIO
                    || $this->page->qtype == LL_VIDEO) {
                echo '<form id="submissionform" action="view.php" method="get">';
                echo '<input type="hidden" name="id" value="'.$this->cmid.'" />';
                echo '<input type="hidden" name="pageid" value="'.$this->page->id.'" />';
                if ($this->lesson->defaultfeedback) { echo '<input type="hidden" name="showfeedback" value="1" />'; }
                echo '</form>';
            }
            //////////////////////////////////////// 

            // if we should show feedback, print it here
            if ($showfeedback) {
                $this->print_feedback($submittedattempt);
            }

            // print the navigation buttons
            $this->print_nav_buttons($savednextpageid, $nomoreattempts);
        }

        // print all remaining informational content for this page
        $this->print_closing_static_content();
        
        // Finish the page
        print_footer($this->course);
    }


    public function display_eol() {
        // end of lesson reached work out grade
        // NOTE that thanks to code in action/continue.php, user will never
        // actually view the EOL page unless they are out of time or they have
        // answered every question
        
        // Used to check to see if the student ran out of time
        $outoftime = optional_param('outoftime', '', PARAM_ALPHA);

        // update the user's timer record
        $this->eol_update_timer();
        
        // log that they got to the end of the lesson
        add_to_log($this->course->id, "languagelesson", "end", "view.php?id=$this->cmid", $this->lesson->id, $this->cmid);

        $this->print_EOL_page();

        print_footer($this->course);
    }
            

    /**
     * Check a lesson's availability for students based on two conditions:
     *   1) Time-based constraints (open time, deadline, etc.)
     *   2) Any dependencies
     **/
    public function check_ll_availability() {
        if ($this->isstudent) {
            $this->checkTimeConstraints();
            $this->checkDependencies();
        }
    }

    
    /**
     * Check any time-based constraints that may be present on this LL (available time,
     * deadline, etc.); This will print a warning and end the script if LL is unavailable
     * @uses exit()
     * @uses $CFG
     **/
    private function checkTimeConstraints() {
        global $CFG;
        if (($this->lesson->available != 0 and time() < $this->lesson->available) or
            ($this->lesson->deadline != 0 and time() > $this->lesson->deadline)) {  // Deadline restrictions
            
            /// if it's past the deadline
            if ($this->lesson->deadline != 0 and time() > $this->lesson->deadline) {
                $message = get_string('lessonclosed', 'languagelesson', userdate($this->lesson->deadline));
            }
            /// otherwise, the lesson's just straightup closed
            else {
                $message = get_string('lessonopen', 'languagelesson', userdate($this->lesson->available));
            }
            
            /// and print the "it's closed!" message
            languagelesson_print_header($this->cm, $this->course, $this->lesson);
            print_simple_box_start('center');
            echo '<div style="text-align:center;">';
            echo '<p>'.$message.'</p>';
            echo '<div class="lessonbutton standardbutton" style="padding: 5px;"><a href="'.$CFG->wwwroot.'/course/view.php?id='.
                $this->course->id .'">'. get_string('returnto', 'languagelesson', format_string($this->course->fullname, true))
                .'</a></div>';
            echo '</div>';
            print_simple_box_end();
            print_footer($this->course);
            exit();
        }
    }


    /**
     * Check for having met the conditions of any dependencies set for starting this lesson
     **/
    private function checkDependencies() {
        if ($this->lesson->dependency) {
            if ($dependentlesson = get_record('languagelesson', 'id', $this->lesson->dependency)) {
                // lesson exists, so we can proceed            
                $conditions = unserialize($this->lesson->conditions);
                // assume false for all
                $timespent = false;
                $completed = false;
                $gradebetterthan = false;
                // check for the timespent condition
                if ($conditions->timespent) {
                    if ($attempttimes = get_records_select('languagelesson_timer', "userid = $USER->id AND lessonid =
                                $dependentlesson->id")) {
                        // go through all the times and test to see if any of them satisfy the condition
                        foreach($attempttimes as $attempttime) {
                            $duration = $attempttime->lessontime - $attempttime->starttime;
                            if ($conditions->timespent < $duration/60) {
                                $timespent = true;
                            }
                        }
                    } 
                } else {
                    $timespent = true; // there isn't one set
                }

                // check for the gradebetterthan condition
                if($conditions->gradebetterthan) {
                    if ($studentgrades = get_records_select('languagelesson_grades', "userid = $USER->id AND lessonid =
                                $dependentlesson->id")) {
                        // go through all the grades and test to see if any of them satisfy the condition
                        foreach($studentgrades as $studentgrade) {
                            if ($studentgrade->grade >= $conditions->gradebetterthan) {
                                $gradebetterthan = true;
                            }
                        }
                    }
                } else {
                    $gradebetterthan = true; // there isn't one set
                }

                // check for the completed condition
                if ($conditions->completed) {
                    if (count_records('languagelesson_grades', 'userid', $USER->id, 'lessonid', $dependentlesson->id)) {
                        $completed = true;
                    }
                } else {
                    $completed = true; // not set
                }

                $errors = array();
                // collect all of our error statements
                if (!$timespent) {
                    $errors[] = get_string('timespenterror', 'languagelesson', $conditions->timespent);
                }
                if (!$completed) {
                    $errors[] = get_string('completederror', 'languagelesson');
                }
                if (!$gradebetterthan) {
                    $errors[] = get_string('gradebetterthanerror', 'languagelesson', $conditions->gradebetterthan);
                }
                if (!empty($errors)) {  // print out the errors if any
                    languagelesson_print_header($this->cm, $this->course, $this->lesson);
                    echo '<p>';
                    print_simple_box_start('center');
                    print_string('completethefollowingconditions', 'languagelesson', $dependentlesson->name);
                    echo '<p style="text-align:center;">'.implode('<br />'.get_string('and', 'languagelesson').'<br />', $errors).'</p>';
                    print_simple_box_end();
                    echo '</p>';
                    print_footer($this->course);
                    exit();
                }
            }
        }
    }
    

    /**
     * TODO
     * @uses $CFG
     * @uses $USER
     * @uses exit();
     */
    public function display_landing_page() {
        global $CFG, $USER;

        // log that this lesson got started   
        add_to_log($this->course->id, 'languagelesson', 'start', 'view.php?id='. $this->cmid, $this->lesson->id, $this->cmid);

        $grade = get_record('languagelesson_grades', 'lessonid', $this->lesson->id, 'userid', $USER->id);
        
        languagelesson_print_header($this->cm, $this->course, $this->lesson, 'view');
        print_simple_box_start('center');
        echo "<div style=\"text-align:center;\">";

        // if the lesson is incomplete, pull the jumpto value for the most recent attempt
        $hascompleted = languagelesson_is_lesson_complete($this->lesson->id, $USER->id);
        if (! $hascompleted) {
            // pull the most recent attempt
            $lastattempt = end($attempts);
            // then, since end forces $attempts' pointer to the last item, reset it to the first
            reset($attempts);
            
            // now pull the jumpto value of the most recent attempt's answer
            $jumpto = get_field('languagelesson_answers', 'jumpto', 'id', $lastattempt->answerid);
            // and convert the jumpto to a proper page id
            if ($jumpto == 0) {
                // they got it wrong, so jump to that page
                $lastpageseen = $lastattempt->pageid;
            }
            else if ($jumpto == LL_NEXTPAGE) {
                // they got it right, so jump to the next page
                if (!$lastpageseen = get_field('languagelesson_pages', 'nextpageid', 'id', 
                            $lastattempt->pageid)) {
                    // next page was 0, so go to end of lesson
                    $lastpageseen = LL_EOL;
                }
            } else {
                // strange jumpto value, so just feed it straight in
                $lastpageseen = $jumpto;
            }
                
            // now check the most recently-seen branch table, and if it was seen more
            // recently than the above attempt, jump to it instead
            if ($recentBranchTable = languagelesson_get_last_branch_table_seen($this->lesson->id, $USER->id)) {
                if ($recentBranchTable->timeseen > $lastattempt->timeseen) {
                    $lastpageseen = $recentBranchTable->pageid;
                }
            }
        }
        
        // print the appropriate page heading
        print_heading(get_string( (($hascompleted) ? 'oldgradeheader' : 'oldgradeincomplete'), 'languagelesson'));
        
        // pull the ID of the first page in the lesson's order
        $firstpageID = get_field_select('languagelesson_pages', 'id', 'lessonid = '.$this->lesson->id.'
                                                                        and prevpageid = 0');
        
        if ($this->course->showgrades) {
            $a->grade = $grade->grade;
            $a->maxgrade = get_field('languagelesson', 'grade', 'id', $this->lesson->id);
            echo '<p>'.get_string('oldgradethisisyourgrade', 'languagelesson', $a).'</p>';

            if (count_records('languagelesson_manattempts', 'lessonid', $this->lesson->id, 'userid', $USER->id, 'graded', 0)) {
                echo '<p>'.get_string('oldgradehasungraded', 'languagelesson').'</p>';
            }
        }
        
        // print out the default "enter lesson" message if no other message is to be printed
        if ($hascompleted && !$this->lesson->timed) {
            if ($this->lesson->type == LL_TYPE_ASSIGNMENT) {
                echo get_string('oldgradeassignmentmessage', 'languagelesson');
                $buttontext = get_string('oldgradeassignmentbutton', 'languagelesson');
            } else {
                echo get_string('oldgradetestmessage', 'languagelesson');
                $buttontext = get_string('oldgradetestbutton', 'languagelesson');
            }
        }
        
        // check if feedback has been posted; if so, tell student it has
        if ($feedbacks = get_records_select('languagelesson_feedback', "lessonid=".$this->lesson->id." and userid=$USER->id")) {
            
            echo '<br /><br /><br />';
            print_simple_box_start('center');
            //echo '<div>';
            echo get_string('oldgradeyouhavefeedback', 'languagelesson');
            
            // build the list of which pages have feedback posted
            $distinctpageIDs = array();
            foreach ($feedbacks as $feedback) {
                if (!in_array($feedback->pageid, $distinctpageIDs)) {
                    $distinctpageIDs[] = $feedback->pageid;
                }
            }
            
            // then use these to print out links to them
            foreach ($distinctpageIDs as $pid) {
                $thispage = get_record('languagelesson_pages', 'id', $pid);
                echo "<a
                    href=\"$CFG->wwwroot/mod/languagelesson/view.php?id=$this->cmid&amp;pageid=$thispage->id\">$thispage->title</a><br
                    />";
            }
            
            print_simple_box_end();
            //echo '</div>';
            
        }


        // if the lesson was timed, give them the restart option as relevant
        if ($this->lesson->timed) {
            if ($this->lesson->type != LL_TYPE_TEST) {
                print_simple_box('<p style="text-align:center;">'. get_string('leftduringtimed', 'languagelesson') .'</p>',
                        'center');
                echo '<div style="text-align:center;" class="lessonbutton standardbutton">'.
                          '<a href="view.php?id='.$this->cmid.'&amp;pageid='.$firstpageid.'&amp;startlastseen=no">'.
                            get_string('continue', 'languagelesson').'</a></div>';
            } else {
                print_simple_box_start('center');
                echo '<div style="text-align:center;">';
                echo get_string('leftduringtimednoretake', 'languagelesson');
            }   
        }

        // otherwise, if it was incomplete, give them the lastpageseen button
        elseif (! $hascompleted) {
            echo "<p style=\"text-align:center;\">".get_string('youhaveseen','languagelesson').'</p>';
            
            echo '<div style="text-align:center;">';
            echo '<span class="lessonbutton standardbutton">'.
                    '<a href="view.php?id='.$this->cmid.'&amp;pageid='.$lastpageseen.'&amp;startlastseen=yes">'.
                    get_string('yes').'</a></span>&nbsp;&nbsp;&nbsp;';

            $buttontext = get_string('no');
        }
        
        // print the basic jump in at first page (optionally reviewing) button
        if (!$this->lesson->timed || $this->lesson->type == LL_TYPE_TEST) {
            echo "<br /><br /><div class=\"lessonbutton standardbutton\"><a
                href=\"view.php?id=$this->cmid&amp;pageid=$firstpageID".
                (($this->lesson->type == LL_TYPE_TEST) ? "&amp;reviewing=1" : '') . "\">".$buttontext.'</a></div>';
        }
        
        echo '<br /><br /><div class="lessonbutton standardbutton"><a href=\"../../course/view.php?id='.
              $this->course->id.'">'.get_string('returntocourse', 'languagelesson').'</a></div>';
        echo "</div>";
        print_simple_box_end();
        print_footer($this->course);
        exit();
    }


    /**
     * Saves the initial timer record for this user on this LL
     * @uses $USER
     * @returns int $pageid The first page in the lesson
     **/
    public function startLL() {
        global $USER;

        // log that this lesson got started   
        add_to_log($this->course->id, 'languagelesson', 'start', 'view.php?id='. $this->cmid, $this->lesson->id, $this->cmid);

        // get the first page
        if (!$pageid = get_field('languagelesson_pages', 'id', 'lessonid', $this->lesson->id, 'prevpageid', 0)) {
                error('Navigation: first page not found');
        }
        
        
        // save the timer record that they started
        if(!isset($USER->startlesson[$this->lesson->id]) && $this->isstudent) {
            
            $USER->startlesson[$this->lesson->id] = true;
            $startlesson = new stdClass;
            $startlesson->lessonid = $this->lesson->id;
            $startlesson->userid = $USER->id;
            $startlesson->starttime = time();
            $startlesson->lessontime = time();
            
            if (!insert_record('languagelesson_timer', $startlesson)) {
                error('Error: could not insert row into lesson_timer table');
            }
            
            if ($this->lesson->timed) {
                languagelesson_set_message(get_string('maxtimewarning', 'languagelesson', $this->lesson->maxtime), 'center');
            }
        }

        return $pageid;
    }
    

    /**
     * If the page currently being viewed is a structural page, it may have conditional redirect behavior,
     * so check for that here
     * @uses $CFG
     **/
    private function handle_structural_redirects() {
        global $CFG;
        
        // handle an opening cluster page
        if ($this->page->qtype == LL_CLUSTER) {  //this only gets called when a user starts up a new lesson and the first page is
                                                 //a cluster
                                            //page
            // if it's a student, jump within the cluster
            if ($this->isstudent) {
                // get page ID to jump to
                $pageid = languagelesson_cluster_jump($this->lesson->id, $USER->id, $pageid);
                // get page record for that ID
                if (!$page = get_record('languagelesson_pages', 'id', $pageid)) {
                    error('Navigation: the page record not found');
                }
                // relog the viewing of the page
                add_to_log($this->course->id, 'languagelesson', 'view', 'view.php?id='. $this->cmid, $pageid, $this->cmid);
            // if it's an editing user, just move on to the next page
            } else {
                // get the next page
                $pageid = $page->nextpageid;
                if (!$page = get_record('languagelesson_pages', 'id', $pageid)) {
                    error('Navigation: the page record not found');
                }
            }
            
            
        // handle the end of a cluster
        } elseif ($this->page->qtype == LL_ENDOFCLUSTER) {
            /// move on to the EOL or the next page as appropriate
            if ($this->page->nextpageid == 0) {
                $nextpageid = LL_EOL;
            } else {
                $nextpageid = $this->page->nextpageid;
            }
            redirect("$CFG->wwwroot/mod/languagelesson/view.php?id=$this->cmid&amp;pageid=$nextpageid");
            
            
        // handle the end of a branch
        } else if ($this->page->qtype == LL_ENDOFBRANCH) {
            redirect("$CFG->wwwroot/mod/languagelesson/view.php?id=$this->cmid&amp;pageid=".$this->page->nextpageid);
        }

    }
        
        
        
        
    /**
     * TODO
     * @uses $CFG
     * @uses $USER
     */
    private function handle_messages($noanswer) {
        global $CFG, $USER;
        // This is where several messages (usually warnings) are displayed
        // all of this is displayed above the actual page
        
        // get the student's timer information
        $this->timer = new stdClass;
        if($this->isstudent) {
            if (!$this->timer = get_records_select('languagelesson_timer', "lessonid = ".$this->lesson->id.
                        " AND userid = $USER->id", 'starttime')) {
                //error('Error: could not find records'); ///TODO: find what causes this error, and get rid of the causes!  Shouldn't
                                                        //just ignore the error...
                languagelesson_insert_bs_timer($this->lesson->id, $USER->id);
                header('Location: '.$CFG->wwwroot.'/mod/languagelesson/view.php?id='.$this->cmid
                       . '&amp;pageid='.$this->page->id);
                exit();
            } else {
                $this->timer = array_pop($this->timer); // this will get the latest start time record
            }
        }
        
        
        // handle updating the time if coming from the "You have seen..." page
        $startlastseen = optional_param('startlastseen', '', PARAM_ALPHA);
        if ($startlastseen == 'yes') {  // continue a previous test, need to update the clock  (think this option is disabled atm)
            $this->timer->starttime = time() - ($this->timer->lessontime - $this->timer->starttime);
            $this->timer->lessontime = time();
        } else if ($startlastseen == 'no') {  // starting over
            // starting over, so reset the clock
            $this->timer->starttime = time();
            $this->timer->lessontime = time();
        }
            
            
        // for timed lessons, display the clock
        if ($this->lesson->timed) {
            if(! $this->isstudent) {
                languagelesson_set_message(get_string('teachertimerwarning', 'languagelesson'));
            } else {
                $timeleft = ($this->timer->starttime + $this->lesson->maxtime * 60) - time();
                
                if ($timeleft <= 0) {
                    // Out of time
                    languagelesson_set_message(get_string('eolstudentoutoftime', 'languagelesson'));
                    redirect("$CFG->wwwroot/mod/languagelesson/view.php?id=$this->cmid&amp;pageid=".LL_EOL."&amp;outoftime=normal");
                    die; // Shouldn't be reached, but make sure
                } else if ($timeleft < 60) {
                    // One minute warning
                    languagelesson_set_message(get_string('studentoneminwarning', 'languagelesson'));
                }
            }
        }
        
        // and update the clock
        if ($this->isstudent) {
            $this->timer->lessontime = time();
            if (!update_record('languagelesson_timer', $this->timer)) {
                error('Error: could not update lesson_timer table');
            }
        }
        
        
        // print the warning msg for teachers to inform them that cluster and unseen does not work while logged in as a teacher
        if(! $this->isstudent) {
            if (languagelesson_display_teacher_warning($this->lesson->id)) {
                $warningvars->cluster = get_string('clusterjump', 'languagelesson');
                $warningvars->unseen = get_string('unseenpageinbranch', 'languagelesson');
                languagelesson_set_message(get_string('teacherjumpwarning', 'languagelesson', $warningvars));
            }
            // and print the message warning that no attempts will be recorded
            languagelesson_set_message(get_string('teacherrecordswarning', 'languagelesson'));
        }
        
        // if they didn't submit an answer, warn them they need to
        if ($noanswer) {
            languagelesson_set_message(get_string('noanswer', 'languagelesson'));
        }

    }


    /**
     * Sets applicable attempt warning flags
     * - lastattemptwarning : flags if user should see the "This is your last attempt
     *   on this question" message
     * - reviewing : flags if user cannot change answer anymore
     **/
    private function set_attempt_warning_flags() {
        $lastattemptwarning = false;
        $nomoreattempts = false;
        if ($this->lesson->maxattempts > 0 &&  // if maxattempts=0, attempts are unlimited
                $this->isstudent) { // show this only to students
            /// pull the number of times they've already attempted this page
            $numprevattempts = count_records('languagelesson_attempts', 'lessonid', $this->lesson->id,
                                             'userid', $USER->id, 'pageid', $this->page->id);
            /// if this is the "maxattempts"th time, flag to warn them they've only got one more shot
            /// IGNORE the warning if this lesson only has 1 attempt on each question
            if ($this->lesson->maxattempts > 1 && $numprevattempts == $this->lesson->maxattempts-1) {
                $lastattemptwarning = true;
            /// if they've already maxed it, flag to tell them they can't do anything more
            } else if ($numprevattempts >= $this->lesson->maxattempts) {
                $nomoreattempts = true;
            }
        }
        /// if reviewing, don't need to display the other messages 
        if ($this->reviewing) {
            $nomoreattempts = false;
            $lastattemptwarning = false;
        }

        return array($lastattemptwarning, $nomoreattempts);
    }
        
        
    /**
     * TODO
     * @uses $CFG
     */
    private function print_page_head($lastattemptwarning, $nomoreattempts, $edit) {
        global $CFG;

        // basic page setup
        $this->PAGE = page_create_instance($this->lesson->id);
        $this->PAGE->set_lessonpageid($this->page->id);
        $this->pageblocks = blocks_setup($this->PAGE);
        
        $this->leftcolumnwidth  = bounded_number(180, blocks_preferred_width($this->pageblocks[BLOCK_POS_LEFT]), 210);
        $this->rightcolumnwidth = bounded_number(180, blocks_preferred_width($this->pageblocks[BLOCK_POS_RIGHT]), 210);

        if (($edit != -1) and $this->PAGE->user_allowed_editing()) {
            $USER->editing = $edit;
        }

        // Print the page header
        $this->PAGE->print_header();
        
        // print attempt warnings (as flagged above)
        if ($lastattemptwarning) {
            print_heading(get_string('lastattempt', 'languagelesson'));
        } else if ($nomoreattempts) {
            print_heading(get_string('nomoreattempts', 'languagelesson'));
        } else if ($this->reviewing) {
            print_heading(get_string('reviewingtest', 'languagelesson'));
        }
        
        
        // print the ongoing score
        if ($this->lesson->showongoingscore) {
            languagelesson_print_ongoing_score($this->lesson);
        }
        
        
        // print out the start of the page template
        ?>
        <table id="layout-table" cellpadding="0" cellspacing="0">
            <tr>
                <!-- First Column -->
                <?php if (languagelesson_blocks_have_content($this->lesson, $this->PAGE, $this->pageblocks, BLOCK_POS_LEFT)) { ?>
                <td id="left-column" style="width: <?php echo $this->leftcolumnwidth; ?>px;">
                    <?php
                        languagelesson_print_menu_block($this->cmid, $this->lesson);

                        if (!empty($CFG->showblocksonmodpages)) {
                            if ((blocks_have_content($this->pageblocks, BLOCK_POS_LEFT) || $this->PAGE->user_is_editing())) {
                                blocks_print_group($this->PAGE, $this->pageblocks, BLOCK_POS_LEFT);
                            }
                        }
                    ?>
                </td>
                <?php } ?>
                <!-- Start main column -->
                <td id="middle-column" align="center">

                    <?php if ($this->lesson->displayleft) { ?>

                    <a name="maincontent" id="maincontent" title="<?php print_string('anchortitle', 'languagelesson') ?>"></a>

                    <?php }
    }


    private function print_prompt() {
        // print the page's contents (description)
        if ($this->page->qtype == LL_BRANCHTABLE) {
            print_heading(format_string($this->page->title));
        } else {
            // print out the instructions on how to complete the question directly above the question text
            $textid = get_field('languagelesson_qtypes', 'textid', 'id', $this->page->qtype);
            ////////////////////////////////////////
            // HACK ////////////////////////////////
            if ($this->page->qtype == LL_MULTICHOICE) {
                if (! $answers = get_records('languagelesson_answers', 'pageid', $this->page->id)) {
                    $textid = 'LL_DESCRIPTION';
                }
            }
            // END HACK ////////////////////////////
            ////////////////////////////////////////
            // check if this is a multiple choice with multiple correct answers; if so, tag it appropriately
            if ($this->page->qtype == LL_MULTICHOICE && $this->page->qoption) { $textid .= 'multiple'; }
            echo '<div class="instructions">'.
                get_string("{$textid}instructions", 'languagelesson').
                '</div>';
            
        }

        $options = new stdClass;
        $options->noclean = true;

        // print the contents (the question prompt)
        // special case for LL_CLOZE: the prompt should display as containing the answer fields, so it's printed below
        if ($this->page->qtype != LL_CLOZE) {
            print_simple_box('<div class="contents">'.
                            format_text(stripslashes($this->page->contents), FORMAT_MOODLE, $options).
                            '</div>', 'center');
        }
    }

        
    /**
     * See if the page should display any old attempt data
     * @uses $CFG, $USER
     * @param object $submittedattempt Contains data on the attempt previously submitted by the user
     * @return object $oldattempt, string $img The record of the old attempt to display and the image
     *         string to display with the selected answer
     **/
    private function handle_old_attempt($submittedattempt) {
        global $CFG, $USER;

        // if there is an old attempt for this question by this user, pull it so we can pre-select
        // their old answer for them, and flag that we need to do so
        $oldattempt = languagelesson_get_most_recent_attempt_on($this->page->id, $USER->id);
        if (($this->lesson->showoldanswer || $nomoreattempts) && $oldattempt) {
            $this->showoldattempt = true;
        }
        
        // if this a teacher, there's no attempt record stored, so use GET variables provided by continue.php to construct the
        // oldattempt object for printing feedback
        else if (! $this->isstudent && $this->lesson->showoldanswer && $submittedattempt->id) {
            $oldattempt = new stdClass();
            $oldattempt->answerid = $submittedattempt->id;

            // if there is an answer for the $submittedattempt->id, use its data; otherwise, it was a wrong shortanswer/numerical
            // (non hard-limited) type, so use the GET $submittedattempt->text variable to store its data
            $answer = get_record('languagelesson_answers', 'id', $submittedattempt->id);
            if ($answer && $answer->score > 0) {
                $oldattempt->correct = true;
                $oldattempt->useranswer = $answer->answer;
                $this->showoldattempt = true;
            } else if ($submittedattempt->text) {
                $oldattempt->correct = false;
                $oldattempt->useranswer = $submittedattempt->text;
                $this->showoldattempt = true;
            }
            // if neither $answer nor $submittedattempt->text exists, that means they're just viewing the question, they haven't
            // attempted it, so display nothing
        }
    
        // if we are showing old attempts and displaying correct/incorrect info in the left menu, mark the image to show the user
        // next to their previous answer
        $img = '';
        if ($this->showoldattempt && $this->lesson->contextcolors && isset($oldattempt)) {
            $img = '<img src="'.$CFG->wwwroot.
                get_string( (($oldattempt->correct) ? 'iconsrccorrect' : 'iconsrcwrong' ) , 'languagelesson').
                '" width="16" height="16" alt="' . 
                (($oldattempt->correct) ? 'Correct' : 'Incorrect') . '" />';
        }

        return array($oldattempt, $img);
    }


    /**
     * Print out answers for SHORTANSWER type questions
     */
    private function print_answers_shortanswer($oldattempt, $img, $options) {
        print_simple_box_start("center");
        echo '<table id="answertable">';
        if ($this->showoldattempt) {
            $value = 'value="'.s($oldattempt->useranswer).'"';
        } else {
            $value = "";
        }       
        echo '<tr><td style="text-align:center;"><label for="answer">'.get_string('youranswer', 'languagelesson').'</label>'.
            ": <input type=\"text\" id=\"answer\" name=\"answer\" size=\"50\" maxlength=\"200\" $value /> $img \n";
        echo '</td></tr></table>';
        print_simple_box_end();
    }




    /**
     * Print out answers for CLOZE type questions
     */
    private function print_answers_cloze($answers, $oldattempt, $img, $options) {
        print_simple_box_start("center");
        echo '<div class="content">';

        $qtext = format_text(stripslashes($this->page->contents), FORMAT_MOODLE, $options);
        $textchunks = languagelesson_parse_cloze($qtext);

        // save the answers in an array, keyed to their order of appearance
        $keyedAnswers = languagelesson_key_cloze_answers($answers);

        // make sure we parsed correctly
        // $textchunks should be in the form of ( 'text', questionNumber, 'text', questionNumber, ... , 'text' )
        if (count($textchunks) != ((2*count($keyedAnswers)) + 1)) {
            error('Cloze parsing failed! Number of questions and answers did not match.');
        }

        // if showing the old answers, parse them
        if ($this->showoldattempt && (! empty($oldattempt->useranswer))) {
            $oldAnswers = unserialize($oldattempt->useranswer);
        }

        foreach ($textchunks as $item) {
            // if it's a string, then it's a chunk of the question prompt, so print it out
            if (is_string($item)) {
                echo $item;
            }
            // otherwise, it's an int indicating which question should go here, so print the question input
            else {
                $answer = $keyedAnswers[$item];
                // if it's a drop-down type
                if ($answer->flags) {
                    // remove the '=' marking the correct answer
                    $options = preg_replace('/=/', '', $answer->answer);
                    // then comma-separate it
                    $options = explode(',', $options);
                    // and turn it into an array to use in a drop-down menu
                    $responseoptions = array();
                    foreach ($options as $option) {
                        $responseoptions[htmlspecialchars(trim($option))] = $option;
                    }
                    //choose_from_menu ($responseoptions, "response[$answer->id]", $selected);
                    if ($this->showoldattempt && isset($oldAnswers[$item])) {
                        choose_from_menu ($responseoptions, "answer[$item]", $oldAnswers[$item]);
                    } else {
                        choose_from_menu ($responseoptions, "answer[$item]");
                    }
                // otherwise, it's a fill in the blank, so print a blank
                } else {
                    // print out the old attempt if necessary
                    if ($this->showoldattempt && isset($oldAnswers[$item])) {
                        $value = 'value="'.$oldAnswers[$item].'"';
                    } else {
                        $value = '';
                    }
                    echo "<input type=\"text\" size=\"15\" name=\"answer[$item]\" $value />";
                }
            }
        }

        echo '</div>';
        print_simple_box_end();
    }
                
                
                
    /**
     * Print out answers for TRUEFALSE type questions
     */
    private function print_answers_truefalse($answers, $oldattempt, $img, $options) {
        print_simple_box_start("center");
        echo '<table id="answertable">';
        if ($this->lesson->shuffleanswers) { shuffle($answers); }
        $i = 0;
        foreach ($answers as $answer) {
            echo '<tr><td valign="top">';
            if ($this->showoldattempt    && $answer->id == $oldattempt->answerid) {
                $checked = 'checked="checked"';
                $im = $img;
            } else {
                $checked = '';
                $im = '';
            } 
            echo "<input type=\"radio\" id=\"answerid$i\" name=\"answerid\" value=\"{$answer->id}\" $checked />";
            echo "</td><td>";
            echo "<label for=\"answerid$i\">".format_text(trim($answer->answer), FORMAT_MOODLE, $options).'</label>';
            echo "</td><td>$im";
            echo '</td></tr>';
            if ($answer != end($answers)) {
                echo "<tr><td><br /></td></tr>";                            
            }
            $i++;
        }
        echo '</table>';
        print_simple_box_end();
    }
                
                
                
                
    /**
     * Print out answers for MULTICHOICE type questions
     */
    private function print_answers_multichoice($answers, $oldattempt, $img, $options) {
        print_simple_box_start("center");
        echo '<table id="answertable">';

        $i = 0;
        if ($this->lesson->shuffleanswers) { shuffle($answers); }

        foreach ($answers as $answer) {
            echo '<tr><td valign="top">';
            if ($this->page->qoption) {
                $checked = '';
                $im = '';
                if ($this->showoldattempt) {
                    $answerids = explode(",", $oldattempt->useranswer);
                    if (in_array($answer->id, $answerids)) {
                        $checked = ' checked="checked"';
                        $im = $img;
                    } else {
                        $checked = '';
                        $im = '';
                    }
                }
                // more than one answer allowed 
                echo "<input type=\"checkbox\" id=\"answerid$i\" name=\"answer[$i]\" value=\"{$answer->id}\"$checked />";
            } else {
                if ($this->showoldattempt    && $answer->id == $oldattempt->answerid) {
                    $checked = ' checked="checked"';
                    $im = $img;
                } else {
                    $checked = '';
                    $im = '';
                } 
                // only one answer allowed
                echo "<input type=\"radio\" id=\"answerid$i\" name=\"answerid\" value=\"{$answer->id}\"$checked />";
            }
            echo '</td><td>';
            echo "<label for=\"answerid$i\" >".format_text(trim($answer->answer), FORMAT_MOODLE, $options).'</label>'; 
            echo "</td><td>$im";
            echo '</td></tr>';
            if ($answer != end($answers)) {
                echo '<tr><td><br /></td></tr>';
            } 
            $i++;
        }
        echo '</table>';
        print_simple_box_end();
    }
                    
                
                
    /**
     * Print out answers for MATCHING type questions
     */
    private function print_answers_matching($answers, $oldattempt, $img, $options) {
        print_simple_box_start("center");
        echo '<table id="answertable">';

        // don't shuffle answers (could be an option??)
        foreach ($answers as $answer) {
            // get all the response
            if ($answer->response != NULL) {
                $responses[] = trim($answer->response);
            }
        }
        
        $responseoptions = array();
        if (!empty($responses)) {
            if ($this->lesson->shuffleanswers) { shuffle($responses); }
            $responses = array_unique($responses);                     
            foreach ($responses as $response) {
                $responseoptions[htmlspecialchars(trim($response))] = $response;
            }
        }
        if ($this->showoldattempt) {
            $useranswers = explode(',', $oldattempt->useranswer);
            $t = 0;
        }
        foreach ($answers as $answer) {
            if ($answer->response != NULL) {
                echo '<tr><td align="right">';
                echo "<b><label for=\"menuresponse[$answer->id]\">".
                        format_text($answer->answer,FORMAT_MOODLE,$options).
                        '</label>: </b></td><td valign="bottom">';
                    
                if ($this->showoldattempt) {
                    $selected = htmlspecialchars(trim($answers[$useranswers[$t]]->response));  // gets the user's previous
                                                                                                //answer
                    choose_from_menu ($responseoptions, "response[$answer->id]", $selected);
                    $t++;
                } else {
                    choose_from_menu ($responseoptions, "response[$answer->id]");
                }
                echo '</td></tr>';
                if ($answer != end($answers)) {
                    echo '<tr><td><br /></td></tr>';
                } 
            }
        }
        echo '</table>';
        print_simple_box_end();
    }
                
                
                
    /**
     * Print out the jump buttons for the BRANCHTABLE
     * @uses $CFG
     */
    private function print_branchtable_jumps() {
        global $CFG;

        print_simple_box_start("center");
        echo '<table id="branchtable">';

        $branches = get_records('languagelesson_branches', 'parentid', $this->page->id, 'ordering');

        $options = new stdClass;
        $options->para = false;
        $buttons = array();
        $i = 0;
        //foreach ($answers as $answer) {
        foreach($branches as $branch) {
            // Each button must have its own form inorder for it to work with JavaScript turned off
            $button  = "<form id=\"answerform$i\" method=\"post\" action=\"$CFG->wwwroot/mod/languagelesson/lesson.php\">\n".
                       '<div>'.
                       "<input type=\"hidden\" name=\"id\" value=\"$this->cmid\" />\n".
                       "<input type=\"hidden\" name=\"action\" value=\"continue\" />\n".
                       "<input type=\"hidden\" name=\"pageid\" value=\"".$this->page->id."\" />\n".
                       "<input type=\"hidden\" name=\"sesskey\" value=\"".sesskey()."\" />\n".
                       "<input type=\"hidden\" name=\"jumpto\" value=\"$branch->firstpage\" />\n".
                       languagelesson_print_submit_link(strip_tags(format_text($branch->title, FORMAT_MOODLE, $options)),
                               "answerform$i", '', true).
                       '</form>';
            
            $buttons[] = $button;
            $i++;
        }
        
        // Set the orientation
        if ($this->page->layout) {
            $orientation = 'horizontal';
        } else {
            $orientation = 'vertical';
        }
        
        $fullbuttonhtml = "\n<div class=\"branchbuttoncontainer $orientation\">\n" .
                          implode("\n", $buttons).
                          "\n</div>\n";
    
        echo $fullbuttonhtml;

        echo '</table>';
    }
                
                
    /**
     * Print out answer for ESSAY type questions
     * @uses $USER
     */
    private function print_answer_essay($oldattempt) {
        global $USER;

        $value = '';
        
        if ($attempt = languagelesson_get_most_recent_attempt_on($this->page->id, $USER->id)) {
            if (!$manattempt = get_record('languagelesson_manattempts', 'id', $attempt->manattemptid)) {
                error('Retrieved attempt record, but failed to retrieve manual attempt record!');
            }

            // print out the submission (if we should) and any feedback that has been recorded for it
            languagelesson_print_submission_feedback_area($manattempt, $this->page->qtype, $this->showoldattempt);
            
            // if the lesson is set to show the old attempt, plug their old submission into the WYSIWYG
            if ($this->showoldattempt) {
                $value = $manattempt->essay;
            }
        }
        
        print_simple_box_start("center");
        echo '<table id="answertable">';

        // use the HTML editor instead of a plain textarea, so students have
        // HTML capability in responses
        $usehtmleditor = can_use_html_editor();
        echo '<tr><td style="text-align:center;" valign="top" nowrap="nowrap">';
        print_textarea($usehtmleditor, 15, 60, 0, 0, 'answer', $value);
        if ($usehtmleditor) { use_html_editor('answer'); }
        echo '</td></tr></table>';
        
        print_simple_box_end();
    }
                
                
                
    /**
     * Print out the recording interface for AUDIO or VIDEO questions
     * @uses $CFG
     * @uses $USER
     */
    private function print_av_interface($oldattempt) {
        global $CFG, $USER;

        // if they're running Windows, warn them the plugin might not work properly
        $browser = get_browser($_SERVER['HTTP_USER_AGENT']);
        if ($browser->platform == "WinXP" || $browser->platform == "WinVista" || $browser->platform == "Win7") {
            echo "<p>Our servers show that your computer is running Windows.  Please be aware that this plugin may be
                unstable on Windows, and for best results, we
                            suggest viewing this page on a Mac.</p>";
        }
        
        if ($oldattempt) {
            error_log(print_r($oldattempt, true));
            if (!$manattempt = get_record('languagelesson_manattempts', 'id', $oldattempt->manattemptid)) {
                error('Failed to retrieve corresponding manual attempt record for this attempt.');
            }
            $this->showoldattempt = true; //there's a stored submission, so enable continuing
            
            // print out the submission and any feedback that has been recorded for it
            languagelesson_print_submission_feedback_area($manattempt, $this->page->qtype);
        }
        
        // print the recording revlet in its own box
        print_simple_box_start('center');

        // pull in the header template
        if ($this->page->qtype == LL_AUDIO) {
            include('runrev/audio/revA.php');
        } else {
            include('runrev/video/revA.php');
        }
        
        // print dynamic parameters
        echo "\t\tMoodleSession=\"". $_COOKIE['MoodleSession'] . "\"\n" ;  
        echo "\t\tMoodleSessionTest=\"" . $_COOKIE['MoodleSessionTest'] . "\"\n";
        echo "\t\tMOODLEID=\"" . $_COOKIE['MOODLEID_'] . "\"\n"; 
        echo "\t\tsesskey=\"" . sesskey() . "\"\n"; 
        echo "\t\tid=\"" . $this->lesson->id . "\"\n"; 
        echo "\t\tpageid=\"" . $this->page->id . "\"\n";
        echo "\t\tpageURL=\"" . languagelesson_get_current_page_url() . "\"\n";
        echo "\t\tuploadtarget=\"" . $CFG->wwwroot . "/mod/languagelesson/upload.php\"\n"; 
        echo "\t\tuploadhost=\"" . $_SERVER['HTTP_HOST'] . "\"\n";
        echo "\t\tuploadpath=\"".preg_replace('/[^\/]*\.php/', '', $_SERVER['SCRIPT_NAME'])."upload.php\"\n";
        echo "\t\tuploadparams=\"id=" . $this->cmid . "&pageid=" . $this->page->id . "&userid=" . $USER->id . "&sesskey=" .
            sesskey() . "\"\n";
        echo "\t\tsubmitscript=\"document.forms['answerform'].submit();\"\n";
        
        /// pull in the ending template
        include($CFG->dirroot . "/mod/languagelesson/runrev/revB.php");
                        
        if (!$newpageid = get_field("languagelesson_pages", "nextpageid", "id", $this->page->id)) {
            // this is the last page - flag end of lesson
            $newpageid = LL_EOL;
        }

        print_simple_box_end();
    }
                
                
    /**
     * Print out any feedback string (custom per-page or default per-LL) appropriate for the student's
     * recorded attempt
     */
    private function print_feedback($submittedattempt) {
        print_simple_box_start('center');

        // if this is an autogradable page, check the correctness of the submission
        if ($this->page->qtype != LL_AUDIO
                && $this->page->qtype != LL_VIDEO
                && $this->page->qtype != LL_ESSAY) {
            // if this is a student, use the attempt that was just submitted
            if ($this->isstudent) {
                if (!$submittedattempt->id || !$attempt = get_record('languagelesson_attempts', 'id', $submittedattempt->id)) {
                    error('Failed to retrieve attempt record for feedback.');
                }
                // set the default feedback message according to if the attempt was correct
                if ($attempt->correct) { $feedback = $this->lesson->defaultcorrect; }
                else { $feedback = $this->lesson->defaultwrong; }
                // pull the record for the answer they chose; if there isn't one, then they submitted something that's not
                // stored as a predicted answer, so create an answer frame object for a wrong answer
                if (! $answer = get_record('languagelesson_answers', 'id', $attempt->answerid)) {
                    $answer = new stdClass();
                    $answer->score = 0;
                    $answer->response = '';
                }
            }
            // otherwise, it's a teacher, so skip the attempt stuff
            else {
                // if there's no answer to pull, create a blank incorrect answer for use in printing feedback message below
                if (!$submittedattempt->id || !$answer = get_record('languagelesson_answers', 'id', $submittedattempt->id)) {
                    $answer = new stdClass();
                    $answer->score = 0;
                    $answer->response = '';
                }
                // set default feedback according to score of answer
                if ($answer->score > 0) { $feedback = $this->lesson->defaultcorrect; }
                else { $feedback = $this->lesson->defaultwrong; }
            }
            // and if that answer has custom feedback, it overrides the default
            $customresponse = trim($answer->response);
            if (!empty($customresponse)) { $feedback = $customresponse; }
            // print the colored bar 
            if (($this->isstudent && $attempt->correct)
                    || (! $this->isstudent && $answer->score > 0)) {
                echo '<div class="feedbackbar greenbar">'.get_string('correct', 'languagelesson').'</div>';
            } else { echo '<div class="feedbackbar redbar">'.get_string('incorrect', 'languagelesson').'</div>'; }
            // print the feedback
            echo "<div>$feedback</div>";
        }
        // if it's a manually-graded page, tell them it'll get graded later
        else {
            echo '<div class="feedbackbar graybar">'.get_string('submitted', 'languagelesson').'</div>';
            echo '<div>'.get_string('waitingforfeedback', 'languagelesson').'</div>';
        }

        print_simple_box_end();
    }


    private function print_nav_buttons($savednextpageid, $nomoreattempts) {
        // print the start of the containing table for the nav buttons
        echo '<table><tr>';

        // if not a revlet page (revlets have own submit buttons), print the submit button
        if ($this->page->qtype != LL_AUDIO && $this->page->qtype != LL_VIDEO
                && !$nomoreattempts && !$this->reviewing) {
            echo '<td>';
            languagelesson_print_submit_link(get_string('submit', 'languagelesson'), 'answerform',
                    "document.forms['answerform'].submit();");
            echo '</td>';
        }
        
        // if there is an old attempt being displayed, print the continue button
        if ($this->showoldattempt || $nomoreattempts || $this->reviewing) {
            echo '<td>';
            echo '<form id="continueform" action="view.php" method="get">';
            echo '<input type="hidden" name="id" value="'.$this->cmid.'" />';

            echo '<input type="hidden" name="pageid" value="';
            // if continue.php output a nextpageid, print it here
            if ($savednextpageid) { echo $savednextpageid; }
            // otherwise, if the nextpageid is a page, point to it
            else if ($this->page->nextpageid) { echo $this->page->nextpageid; }
            // otherwise, this page is the end of the lesson, so if the lesson is complete, just let continuing go back to
            // the old grade landing page; if it's not complete, direct continue to an unanswered page
            else if (! languagelesson_is_lesson_complete($this->lesson->id, $USER->id)) {
                echo languagelesson_find_first_unanswered_pageid($this->lesson->id, $USER->id);
            }

            // if they're reviewing, keep them reviewing
            if ($this->reviewing) { echo '<input type="hidden" name="reviewing" value="1" />'; }

            /// put the buttons in a table outside the form, make the submit button just submit the answerform from outside

            languagelesson_print_submit_link(get_string('continue', 'languagelesson'), 'continueform');
            echo '</form>';
            echo '</td>';
        }

        echo '</tr></table>';
    }
        
        

    /**
     * TODO
     * @uses $CFG
     */
    private function handle_no_answers() {
        global $CFG;

        // a page without answers - find the next (logical) page
        echo "<form id=\"pageform\" method=\"get\" action=\"$CFG->wwwroot/mod/languagelesson/view.php\">\n";
        echo '<div>';
        echo "<input type=\"hidden\" name=\"id\" value=\"$this->cmid\" />\n";
        
        if (!$newpageid = get_field("languagelesson_pages", "nextpageid", "id", $this->page->id)) {
            // this is the last page - flag end of lesson
            $newpageid = LL_EOL;
        }
        echo "<input type=\"hidden\" name=\"pageid\" value=\"$newpageid\" />\n";
        languagelesson_print_submit_link(get_string('continue', 'languagelesson'), 'pageform');
        echo "</form>\n";
    }
        
        
    /**
     * Print all remaining informational content;
     * - progress bar
     * - side blocks
     * @uses $CFG
     */
    private function print_closing_static_content() {
        global $CFG;

        languagelesson_print_progress_bar($this->lesson, $this->course);

        // print the "Report a problem" link for revlets
        echo '<a href="https://docs.google.com/a/carleton.edu/spreadsheet/viewform?formkey=dGw5bjNrN2tjS3MwbC05NnVnNV9HZFE6MQ"
            target="_blank" style="font-size:0.75em; margin-top:25px;">Report a problem</a>';
        ?>
                </td>
                <?php if (languagelesson_blocks_have_content($this->lesson, $this->PAGE, $this->pageblocks, BLOCK_POS_RIGHT)) { ?>
                <td id="right-column" style="width: <?php echo $this->rightcolumnwidth; ?>px;">
                    <?php
                        languagelesson_print_clock_block($this->cmid, $this->lesson, $this->timer);
                        languagelesson_print_mediafile_block($this->cmid, $this->lesson);

                        if (!empty($CFG->showblocksonmodpages)) {
                            if ((blocks_have_content($this->pageblocks, BLOCK_POS_RIGHT) || $this->PAGE->user_is_editing())) {
                                blocks_print_group($this->PAGE, $this->pageblocks, BLOCK_POS_RIGHT);
                            }
                        }
                    ?>
                </td>
                <?php } ?>
            </tr>
        </table><?php
    }
    
    
        
    /**
     * TODO
     * @uses CFG
     * @uses USER
     */
    private function eol_update_timer() {
        global $CFG, $USER;

        if ($this->isstudent) {
            unset($USER->startlesson[$this->lesson->id]);
            if (!$timer = get_records_select('languagelesson_timer', "lessonid =".$this->lesson->id." AND userid = $USER->id",
                        'starttime')) {
                //error('Error: could not find records'); ///TODO: find what causes this error, and get rid of the causes!  Shouldn't
                    //just ignore the error...
                languagelesson_insert_bs_timer($this->lesson->id, $USER->id);
                header('Location: '.$CFG->wwwroot.'/mod/languagelesson/view.php?id='.$this->cmid."&amp;pageid=".$this->page->id);
                exit();
            } else {
                $timer = array_pop($timer); // this will get the latest start time record
            }
            $timer->lessontime = time();
            
            if (!update_record("languagelesson_timer", $timer)) {
                error("Error: could not update lesson_timer table");
            }
        }
    }
        
        
    /**
     * TODO
     * @uses CFG
     * @uses $USER
     */
    private function print_EOL_page() {
        global $CFG, $USER;

        // start printing EOL report page
        languagelesson_print_header($this->cm, $this->course, $this->lesson, 'view');
        print_heading(get_string("congratulations", "languagelesson"));
        print_simple_box_start("center");
        
        
        // if this is a student, grade the lesson and print out grade information
        if ($this->isstudent) {
            
            if (! $oldgrade = get_record("languagelesson_grades", 'lessonid', $this->lesson->id, 'userid', $USER->id)) {
                error("Could not fetch old grade record.");
            }
            
            // if the old grade is marked as completed, redirect the user to the "Old Grade" page
            if ($oldgrade->completed) {
                redirect("$CFG->wwwroot/mod/languagelesson/view.php?id=$this->cmid", '', 0);
            }
            
            // grade the lesson
            // @TODO@ this is redundant, as action/continue now grades the lesson
            //        after every question submission; however, need the object
            //        produced by languagelesson_grade here
            $gradeinfo = languagelesson_grade($this->lesson);
            
            // build the grade object used to mark that this lesson was actually completed
            $gradeobj = new stdClass();
            $gradeobj->id = $oldgrade->id;
            $gradeobj->lessond = $this->lesson->id;
            $gradeobj->userid = $USER->id;
            $gradeobj->grade = $gradeinfo->grade;
            $gradeobj->completed = time();
            
            // and update the record
            if (! $update = update_record("languagelesson_grades", $gradeobj)) {
                error("Grade not updated.");
            }
            
            
            // and print out information on the grade they got
            // print this ONLY if this course allows students to see their grades
            if ($gradeinfo->nanswered && $this->course->showgrades) { //changed from if($gradeinfo->attempts)
                // build the message to show to the student
                $a = new stdClass;
                $a->earned = $gradeinfo->grade;
                $a->total = get_field('languagelesson', 'grade', 'id', $this->lesson->id);
                
                // if there were manually-graded questions in the lesson, let the student know that
                // the grade they have now is NOT necessarily their final grade
                // ignore this if the lesson was set to autograde manual-type questions
                if ($gradeinfo->nmanual && !$this->lesson->autograde) {
                    $a->tempmaxgrade = $a->total - $gradeinfo->manualpoints;
                    $a->nmanquestions = $gradeinfo->nmanual;
                    echo "<div style=\"text-align:center;\">".get_string("displayscorewithmanuals",
                        "languagelesson", $a)."</div>";
                } else {
                    echo "<div style=\"text-align:center;\">".get_string("displayscorewithoutmanuals", "languagelesson", $a)."</div>";  
                }
                
            } else {
                if ($this->lesson->timed) {
                    if ($outoftime == 'normal') {
                        echo get_string("eolstudentoutoftimenoanswers", "languagelesson");
                    }
                } else {
                    echo get_string("welldone", "languagelesson");
                }
            }

        } else { 
            // display for teacher
            echo "<p style=\"text-align:center;\">".get_string("displayofgrade", "languagelesson")."</p>\n";
        }
        print_simple_box_end(); //End of Lesson button to Continue.
        
        
        // if this lesson links to another activity, print out that link now
        if ($this->lesson->activitylink) {
            if ($module = get_record('course_modules', 'id', $this->lesson->activitylink)) {
                if ($modname = get_field('modules', 'name', 'id', $module->module))
                    if ($instance = get_record($modname, 'id', $module->instance)) {
                        echo "<div style=\"text-align:center; padding:5px;\" class=\"lessonbutton standardbutton\">".
                                "<a href=\"$CFG->wwwroot/mod/$modname/view.php?id=$this->lesson->activitylink\">".
                                get_string('activitylinkname', 'languagelesson', $instance->name)."</a></div>\n";
                    }
            }
        }
        
    
        // print out course and gradebook links
        $courseid = $this->course->id;
        echo "<div style=\"text-align:center; padding:5px;\" class=\"lessonbutton standardbutton\"><a
            href=\"$CFG->wwwroot/course/view.php?id=$courseid\">".get_string('returnto', 'languagelesson',
                    format_string($this->course->fullname, true))."</a></div>\n";
        if ($this->course->showgrades) {
            echo "<div style=\"text-align:center; padding:5px;\" class=\"lessonbutton standardbutton\"><a
                href=\"$CFG->wwwroot/grade/index.php?id=$courseid\">".get_string('viewgrades', 'languagelesson')."</a></div>\n";
        }
    }

}

?>
