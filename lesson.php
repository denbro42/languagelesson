<?php  // $Id: lesson.php 637 2011-07-19 16:02:21Z griffisd $
/**
 * Handles lesson actions
 * 
 * ACTIONS handled are:
 *    addbranchtable
 *    addendofbranch
 *    addcluster
 *    addendofcluster
 *    addpage
 *    confirmdelete
 *    continue
 *    delete
 *    editpage
 *    insertpage
 *    move
 *    moveit
 *    updatepage
 * @version $Id: lesson.php 637 2011-07-19 16:02:21Z griffisd $
 * @license http://www.gnu.org/copyleft/gpl.html GNU Public License
 * @package lesson
 **/

    require("../../config.php");
    require("locallib.php");
    
    $id     = required_param('id', PARAM_INT);         // Course Module ID
    $action = required_param('action', PARAM_ALPHA);   // Action
    
    list($cm, $course, $lesson) = languagelesson_get_basics($id);

    require_login($course, false, $cm);
    $context = get_context_instance(CONTEXT_MODULE, $cm->id);
    
/// Set up some general variables
    $usehtmleditor = can_use_html_editor();
    
/// Process the action
    switch ($action) {
        case 'addbranchtable':
        case 'addpage':
        case 'confirmdelete':
        case 'editpage':
        case 'move':
            languagelesson_print_header($cm, $course, $lesson);
        case 'addcluster':
        case 'addendofbranch':
        case 'addendofcluster':
        case 'delete':
        case 'insertpage':
        case 'updatepage':
            require_capability('mod/languagelesson:edit', $context);
        case 'continue':
            include($CFG->dirroot.'/mod/languagelesson/action/'.$action.'.php');
            break;
        default:
            error("Fatal Error: Unknown action\n");
    }

    print_footer($course);
 
?>
