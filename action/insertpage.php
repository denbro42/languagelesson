<?php // $Id: insertpage.php 677 2011-10-12 18:38:45Z griffisd $
/**
 * Action for processing the form from addpage action and inserts the page.
 *
 * @version $Id: insertpage.php 677 2011-10-12 18:38:45Z griffisd $
 * @license http://www.gnu.org/copyleft/gpl.html GNU Public License
 * @package languagelesson
 **/

require_sesskey();

// pull in the insertion library
require('insert_class.php');

// check to see if the cancel button was pushed
if (optional_param('cancel', '', PARAM_ALPHA)) {
    redirect("$CFG->wwwroot/mod/languagelesson/edit.php?id=$cm->id");
}

$timenow = time();

$form = data_submitted();

$inserter = new languagelesson_page_inserter();
$newpage = $inserter->insert($lesson, $form, $timenow);

languagelesson_set_message(get_string('insertedpage', 'languagelesson').': '.format_string($newpage->title, true), 'notifysuccess');
redirect("$CFG->wwwroot/mod/languagelesson/edit.php?id=$cm->id");

?>
