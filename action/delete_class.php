<?php
/**
 * Class definition for page deletion action
 *
 * @version 
 * @license http://www.gnu.org/copyleft/gpl.html GNU Public License
 * @package languagelesson
 */

require_once(dirname(__FILE__).'/../../../config.php');
require_once($CFG->dirroot.'/mod/languagelesson/lib.php');
require_once($CFG->dirroot.'/mod/languagelesson/locallib.php');

class languagelesson_page_deleter {

	// declare a lesson object for this class, just to have a placeholder
	private $lesson = null;


    function __construct($lesson) {
        $this->lesson = $lesson;
    }


	/*
	 * Delete the hell out of a page.
	 * @param int/object $delpage The page record (or the ID of it) to delete.
	 */
	public function delete_page($delpage) {
        // function allows for flexible input, so if an ID int is given, fetch the record; if a record is given directly, no need
        // to query the DB
        if (gettype($delpage) == "integer") {
            if (!$delpage = get_record("languagelesson_pages", "id", $delpage)) {
                error("Delete: page record not found");
            }
        }

		$this->delete_page_data($delpage);

		// set the prevpageid and nextpageid values to use in patching the hole in the linked list
		$prevpageid = $delpage->prevpageid;
		// set nextpageid based on the type of the page
		switch ($delpage->qtype) {
			// if it's an end of branch, the nextpageid pointer will vary, so pull the id of the page whose previous pointer is this
			// one
			case LL_ENDOFBRANCH:
				$nextpageid = get_field('languagelesson_pages', 'id', 'prevpageid', $delpage->id);
				break;
			// if it's a branch table, we've just changed the nextpageid value, so pull it again
			case LL_BRANCHTABLE:
				$nextpageid = get_field('languagelesson_pages', 'nextpageid', 'id', $delpage->id);
				break;
			// otherwise, just take the one we got with the page
			default;
				$nextpageid = $delpage->nextpageid;
				break;
		}


		// now delete the page itself
		$this->delete_from('pages', 'id', $delpage->id, 'could not delete page record');

		// repair the hole in the linkage
		$this->repairLinkedList($prevpageid, $nextpageid);

		// and update ordering values
		// if we deleted a Branch Table, a variable number of pages was deleted and branchids are no longer valid, so just rebuild the
		// ordering and branchid values for the lesson
		if ($delpage->qtype == LL_BRANCHTABLE) {
			languagelesson_update_ordering($this->lesson->id);
		}
		// otherwise, can just linearly adjust all of the following ordering values
		else {
			if ($changePages = get_records_select('languagelesson_pages', "ordering > $delpage->ordering", 'ordering')) {
				foreach ($changePages as $page) {
					$this->setTo('pages', 'ordering', $page->ordering-1, 'id', $page->id, 'ordering value');
				}
			}
		}

		// if this was the first page in a branch, update the firstpage value for that branch
		if ($delpage->branchid && $delpage->id == get_field('languagelesson_branches', 'firstpage', 'id', $delpage->branchid)) {
			if(! set_field('languagelesson_branches', 'firstpage', $delpage->nextpageid, 'id', $delpage->branchid)) {
				error("Delete page: could not update firstpage value of branch");
			}
		}

		// update the lesson's calculated max grade
		languagelesson_recalculate_maxgrade($this->lesson->id);

	}


	/*
	 * Deletes all relevant data related to the delpage, based on its type
	 * @param object $delpage The page record to delete associated data for
	 */
	private function delete_page_data($delpage) {
		switch ($delpage->qtype) {

			// These pages have no associated content
			case LL_CLUSTER:
			case LL_ENDOFCLUSTER:
			case LL_ENDOFBRANCH:
			break;
			
			// Need to delete ENDOFBRANCH records, seen branches, and branches
			case LL_BRANCHTABLE:
				$branches = get_records('languagelesson_branches', 'parentid', $delpage->id, 'ordering');
				foreach ($branches as $branch) {
					// delete the branch's ENDOFBRANCH page
					$eob = get_record('languagelesson_pages', 'qtype', LL_ENDOFBRANCH, 'branchid', $branch->id);
					$this->delete_page($eob);

					// delete any related seenbranch records and the branches themselves
					$this->delete_from('seenbranches', 'id', $branch->id, 'could not delete seen branch records');
					$this->delete_from('branches', 'id', $branch->id, 'could not delete branch record');

					// and reset the branch IDs for every page that was in this branch
					if ($branchpages = get_records('languagelesson_pages', 'branchid', $eob->branchid)) {
						foreach($branchpages as $bpid => $bp) {
							$this->setTo('pages', 'branchid', $delpage->branchid, 'id', $bpid, 'could not reset branchid');
						}
					}
				}
			break;

			// otherwise, it's a question page, so get rid of attempt data
			default:
				// student-submitted data
				$this->delete_from('attempts', 'pageid', $delpage->id, 'could not delete attempt records');
				$this->delete_from('manattempts', 'pageid', $delpage->id, 'could not delete manual attempt records');
				$this->delete_from('feedback', 'pageid', $delpage->id, 'could not delete feedback records');
				if (! languagelesson_delete_user_files($this->lesson, $delpage->id)) {
					error('Deleting page: could not delete submitted files!');
				}
				// question-specific data
				$this->delete_from('answers', 'pageid', $delpage->id, 'could not delete answer records');
			break;
		}
	}


	/*
	 * Patches the hole left in the prevpageid/nextpageid linked list structure left by removing the delpage
	 * @param int $prevpageid The ID of the page directly before the delpage
	 * @param int $nextpageid The ID of the page directly succeeding the delpage
	 */
	private function repairLinkedList($prevpageid, $nextpageid) {
		if (!$prevpageid AND !$nextpageid) {
			//This is the only page, no repair needed
		} elseif (!$prevpageid) {
			// this is the first page...
			$this->setTo('pages', 'prevpageid', 0, 'id', $nextpageid, 'prevpage link');
		} elseif (!$nextpageid) {
			// this is the last page...
			$this->setTo('pages', 'nextpageid', 0, 'id', $prevpageid, 'nextpage link');
		} else {
			// page is in the middle...
			$this->setTo('pages', 'nextpageid', $nextpageid, 'id', $prevpageid, 'next link');
			$this->setTo('pages', 'prevpageid', $prevpageid, 'id', $nextpageid, 'previous link');
		}
	}


	// these two functions allow for very specific error-checking but keep the code below brief
	private function delete_from($table, $field, $val, $error) {
		if (!delete_records("languagelesson_$table", $field, $val)) {
			error("Delete page: $error");
		}
	}
	private function setTo($table, $setfield, $setval, $findfield, $findval, $error) {
        if (!set_field("languagelesson_$table", $setfield, $setval, $findfield, $findval)) {
            error("Delete: unable to set $error");
        }
	}

}

?>
