<?php // $Id: delete.php 677 2011-10-12 18:38:45Z griffisd $
/**
 * Action for deleting a page
 *
 * @version $Id: delete.php 677 2011-10-12 18:38:45Z griffisd $
 * @license http://www.gnu.org/copyleft/gpl.html GNU Public License
 * @package lesson
 **/

require_sesskey();

require_once('lib.php');
require_once('locallib.php');

// pull in the deletion library
require('delete_class.php');

// pull the page to be deleted it
$pageid = required_param('pageid', PARAM_INT);

// delete the page
$deleter = new languagelesson_page_deleter($lesson);
$deleter->delete_page($pageid);

// and go back to the interface
languagelesson_set_message(get_string('deletedpage', 'languagelesson').': '.format_string($delPage->title, true), 'notifysuccess');
redirect("$CFG->wwwroot/mod/languagelesson/edit.php?id=$cm->id");

?>
