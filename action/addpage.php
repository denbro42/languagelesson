<?php // $Id: addpage.php 677 2011-10-12 18:38:45Z griffisd $
/**
 *  Action for adding a question page.  Prints an HTML form.
 *
 * @version $Id: addpage.php 677 2011-10-12 18:38:45Z griffisd $
 * @license http://www.gnu.org/copyleft/gpl.html GNU Public License
 * @package languagelesson
 **/
    $CFG->pagepath = 'mod/languagelesson/addpage';
    
    // first get the preceding page
    $pageid = required_param('pageid', PARAM_INT);
    $qtype = optional_param('qtype', LL_MULTICHOICE, PARAM_INT);
    
    // set of jump array
    $jump = array();
    $jump[0] = get_string("thispage", "languagelesson");
    $jump[LL_NEXTPAGE] = get_string("nextpage", "languagelesson");
    $jump[LL_PREVIOUSPAGE] = get_string("previouspage", "languagelesson");
    $jump[LL_EOL] = get_string("endoflesson", "languagelesson");
    if(languagelesson_display_branch_jumps($lesson->id, $pageid)) {
        $jump[LL_UNSEENBRANCHPAGE] = get_string("unseenpageinbranch", "languagelesson");
        $jump[LL_RANDOMPAGE] = get_string("randompageinbranch", "languagelesson");
    }
    if(languagelesson_display_cluster_jump($lesson->id, $pageid)) {
        $jump[LL_CLUSTERJUMP] = get_string("clusterjump", "languagelesson");
    }
    if (!optional_param('firstpage', 0, PARAM_INT)) {
        $linkadd = "";      

		$pages = get_records('languagelesson_pages', 'lessonid', $lesson->id, 'ordering');
		foreach ($pages as $apage) {
			$jump[$apage->id] = strip_tags(format_string($apage->title,true));
        }
    } else {
        $linkadd = "&amp;firstpage=1";
    }

	// they may have switched question types or added answer fields, so pull anything that was submitted
    $data = data_submitted();

	// set the number of answer fields according to the submitted data
	// defaults to 4
	if (isset($data->maxanswers)) {
		$maxanswers = $data->maxanswers + 4;
	} else {
		$maxanswers = 4;
	}

    // give teacher a blank proforma
    print_heading_with_help(get_string("addaquestionpage", "languagelesson"), "overview", "languagelesson");
    ?>
    <form id="form" method="post" action="lesson.php" class="addform">
    <fieldset class="invisiblefieldset fieldsetfix">
    <input type="hidden" name="id" value="<?php echo $cm->id ?>" />
    <input type="hidden" name="action" id="actioninput" />
    <input type="hidden" name="pageid" value="<?php echo $pageid ?>" />
	<!-- print out a hidden input for adding more answer fields -->
	<input type="hidden" name="maxanswers" value="<?php echo $maxanswers; ?>" />
    <input type="hidden" name="sesskey" value="<?php echo $USER->sesskey ?>" />
      <?php
        echo '<b>'.get_string("questiontype", "languagelesson").":</b> \n";
        echo helpbutton("questiontypes", get_string("questiontype", "languagelesson"), "languagelesson")."<br />";
        languagelesson_qtype_menu($LL_QUESTION_TYPE, $qtype, 
                          "lesson.php?id=$cm->id&amp;action=addpage&amp;pageid=".$pageid.$linkadd);

		// display the qoption checkbox for those question types that require it
        if ( $qtype == LL_SHORTANSWER
				|| $qtype == LL_MULTICHOICE 
				|| $qtype == LL_CLOZE ) {
            echo '<p>';
            if ($qtype == LL_SHORTANSWER) {
                $qoptionstr = get_string('casesensitive', 'languagelesson');
            } else if ($qtype == LL_MULTICHOICE) {
                $qoptionstr = get_string('multianswer', 'languagelesson');
            } else {
				$qoptionstr = get_string('casesensitive', 'languagelesson');
			}
            echo "<label for=\"qoption\"><strong>$qoptionstr</strong></label>";
			echo "<input type=\"checkbox\" id=\"qoption\" name=\"qoption\" value=\"1\" "
				. ((isset($data->qoption) && $data->qoption) ? "checked=\"checked\" " : '') . "/>";
            helpbutton("questionoption", get_string("questionoption", "languagelesson"), "languagelesson");
            echo '</p>';
        }
    ?>
    <table cellpadding="5" class="generalbox boxaligncenter" border="1">
    <tr valign="top">
    <td><b><label for="title"><?php print_string("pagetitle", "languagelesson"); ?>:</label></b><br />
    <input type="text" id="title" name="title" size="80" value="<?php if (isset($data->title)) { echo $data->title; } ?>" /></td></tr>
    <?php
    echo "<tr><td><b>";
    echo get_string("pagecontents", "languagelesson").":</b><br />\n";
    print_textarea($usehtmleditor, 20, 70, 630, 400, "contents", ((isset($data->contents)) ? $data->contents : ''));
    if ($usehtmleditor) {
        use_html_editor("contents");
    }
    echo "</td></tr></table>\n";
	?>

    <table cellpadding="5" class="generalbox boxaligncenter" border="1">
	<?php
	// for brevity, store this lesson's defaultpoints value here
	$defaultpoints = get_field('languagelesson', 'defaultpoints', 'id', $lesson->id);
	// now print out answer/feedback/score fields
    switch ($qtype) {
        case LL_TRUEFALSE :
            for ($i = 0; $i < 2; $i++) {
                $iplus1 = $i + 1;
                echo "<tr><td>";
				echo '<table><tr><td class="answerrow_cell">';
				echo "<b>".get_string("answer", "languagelesson")." $iplus1:</b><br />\n";
                print_textarea(false, 1, 30, 0, 0, "answer[$i]", ((isset($data->answer[$i]) ? $data->answer[$i] : '')));
                echo "</td><td class=\"answerrow_cell\">\n";
				echo '<b>'.get_string("score", "languagelesson")." $iplus1:</b><br />";
				echo "<input type=\"text\" name=\"score[$i]\" value=\"".((isset($data->score[$i])) ? $data->score[$i] :
						(($i) ? '0' : $defaultpoints))."\" size=\"5\" />";
				echo "</td><td class=\"answerrow_cell\">\n";
                echo "<b>".get_string("response", "languagelesson")." $iplus1:</b><br />\n";
                print_textarea(false, 1, 30, 0, 0, "response[$i]", ((isset($data->response[$i]) ? $data->response[$i] : '')));
                echo "</td><td class=\"answerrow_cell\">\n";
                echo "<b>".get_string("jump", "languagelesson")." $iplus1:</b><br />\n";
				choose_from_menu($jump, "jumpto[$i]", ((isset($data->jumpto[$i])) ? $data->jumpto[$i] : 
						(($i) ? 0 : LL_NEXTPAGE)), "");
                helpbutton("jumpto", get_string("jump", "languagelesson"), "languagelesson");
                echo "</td></tr></table>\n";
				echo '</td></tr>';
            }
            break;
        case LL_AUDIO :
        case LL_VIDEO :
        case LL_ESSAY :
                echo "<tr><td><table>";
				echo '<tr><td class="answerrow_cell"><b>'.get_string("score", "languagelesson").":</b><br />"
				."<input type=\"text\" name=\"score[0]\""
					."value=\"".((isset($data->score[0])) ? $data->score[0] : $defaultpoints)."\" size=\"5\" />";
				echo '</td><td class="answerrow_cell"><b>'.get_string("jump", "languagelesson").":</b><br />\n";
                choose_from_menu($jump, "jumpto[0]", ((isset($data->jumpto[0])) ? $data->jumpto[0] : LL_NEXTPAGE), "");
                helpbutton("jumpto", get_string("jump", "languagelesson"), "languagelesson");
                echo "</td></tr></table></td></tr>\n";
            break;
        case LL_MATCHING :
            for ($i = 0; $i < $maxanswers+2; $i++) {
                $iplus1 = $i + 1;
                if ($i == $maxanswers) {
					// insert a visual break between the answers and the feedback areas
					echo '<tr><td></td></tr>';

                	echo "<tr><td><table>";
					// print the correct feedback area
                    echo '<tr><td class="answerrow_cell"><b>'.get_string("correctresponse", "languagelesson").":</b><br />\n";
                    print_textarea(false, 1, 35, 0, 0, "answer[$i]", ((isset($data->answer[$i]) ? $data->answer[$i] : '')));
                    echo '</td><td class="answerrow_cell">';
					// print the score area
					echo '<b>'.get_string("correctanswerscore", "languagelesson").":</b><br />";
					echo "<input type=\"text\" name=\"score[$i]\" value=\"".((isset($data->score[$i])) ? $data->score[$i] :
							$defaultpoints)."\" size=\"5\" />";
					echo '</td><td class="answerrow_cell">';
                    // print the jump area
                    echo "<b>".get_string("correctanswerjump", "languagelesson").":</b><br />\n";
                    choose_from_menu($jump, "jumpto[$i]", ((isset($data->jumpto[$i])) ? $data->jumpto[$i] : LL_NEXTPAGE), "");
                    helpbutton("jumpto", get_string("jump", "languagelesson"), "languagelesson");
                    echo "</td></tr></table>\n";
					echo '</td></tr>';
                } elseif ($i == $maxanswers+1) {
                    echo "<tr><td><table>";
					// print wrong feedback area
					echo '<tr><td class="answerrow_cell"><b>'.get_string("wrongresponse", "languagelesson").":</b><br />\n";
                    print_textarea(false, 1, 35, 0, 0, "answer[$i]", ((isset($data->answer[$i]) ? $data->answer[$i] : '')));
                    echo '</td><td class="answerrow_cell">';
                    // print score area
					echo '<b>'.get_string("wronganswerscore", "languagelesson").":</b><br />";
					echo "<input type=\"text\" name=\"score[$i]\" value=\"".((isset($data->score[$i])) ? $data->score[$i] :
							'0')."\" size=\"5\" />";
					echo '</td><td class="answerrow_cell">';
					// print wrong answer jump
                    echo "<b>".get_string("wronganswerjump", "languagelesson").":</b><br />\n";
                    choose_from_menu($jump, "jumpto[$i]", ((isset($data->jumpto[$i])) ? $data->jumpto[$i] : 0), "");
                    helpbutton("jumpto", get_string("jump", "languagelesson"), "languagelesson");
                    echo "</td></tr></table>";
					echo '</td></tr>';
                    
                } else {                                                
                    echo "<tr><td><table>";
					echo '<tr><td class="answerrow_cell"><b>'.get_string("answer", "languagelesson")." $iplus1:</b><br />\n";
                    print_textarea(false, 1, 40, 0, 0, "answer[$i]", ((isset($data->answer[$i]) ? $data->answer[$i] : '')));
                    echo '</td><td class="answerrow_cell">';
                    echo "<b>".get_string("matchesanswer", "languagelesson")." $iplus1:</b><br />\n";
                    print_textarea(false, 1, 40, 0, 0, "response[$i]", ((isset($data->response[$i]) ? $data->response[$i] : '')));
                    echo "</td></tr></table>";
					echo '</td></tr>';
                }
            }
            break;
        case LL_SHORTANSWER :
        //case LL_NUMERICAL :
        case LL_MULTICHOICE :
            // default code
            for ($i = 0; $i < $maxanswers; $i++) {
                $iplus1 = $i + 1;
                echo "<tr><td>";
				echo '<table><tr><td class="answerrow_cell">';
				echo "<b>".get_string("answer", "languagelesson")." $iplus1:</b><br />\n";
                print_textarea(false, 1, 30, 0, 0, "answer[$i]", ((isset($data->answer[$i]) ? $data->answer[$i] : '')));
                echo "</td><td class=\"answerrow_cell\">\n";
                if ($i) {
					echo '<b>'.get_string("score", "languagelesson")." $iplus1:</b><br />";
					echo "<input type=\"text\" name=\"score[$i]\" value=\"".((isset($data->score[$i])) ? $data->score[$i] : '0')."\"
						size=\"5\" />";
                } else {
					echo '<b>'.get_string("score", "languagelesson")." $iplus1:</b><br />";
					echo "<input type=\"text\" name=\"score[$i]\" value=\"".((isset($data->score[$i])) ? $data->score[$i] :
							$defaultpoints)."\" size=\"5\" />";
                }
				echo "</td><td class=\"answerrow_cell\">\n";
                echo "<td><b>".get_string("response", "languagelesson")." $iplus1:</b><br />\n";
                print_textarea(false, 1, 30, 0, 0, "response[$i]", ((isset($data->response[$i]) ? $data->response[$i] : '')));
				echo "</td><td class=\"answerrow_cell\">\n";
                echo "<b>".get_string("jump", "languagelesson")." $iplus1:</b><br />\n";
                if ($i) {
                    // answers 2, 3, 4... jumpto this page
                    choose_from_menu($jump, "jumpto[$i]", ((isset($data->jumpto[$i])) ? $data->jumpto[$i] : 0), "");
                } else {
                    // answer 1 jumpto next page
                    choose_from_menu($jump, "jumpto[$i]", ((isset($data->jumpto[$i])) ? $data->jumpto[$i] : LL_NEXTPAGE), "");
                }
                helpbutton("jumpto", get_string("jump", "languagelesson"), "languagelesson");
				echo '</td></tr></table>';
                echo "</td></tr>\n";
            }
            break;
		case LL_CLOZE :
			for ($i = 0; $i < $maxanswers; $i++) {
				$iplus1 = $i + 1;
				echo "<tr><td>";

				echo '<table><tr><td class="answerrow_cell">';
				// print the answer input area
				echo "<b>".get_string("answer", "languagelesson")." $iplus1:</b><br />\n";
				print_textarea(false, 1, 30, 0, 0, "answer[$i]", ((isset($data->answer[$i])
						? $data->answer[$i] : '')));
				echo '</td><td class="answerrow_cell">';
				// print the drop-down checkbox
				$a->number = $iplus1;
				echo "<label for=\"dropdown[$i]\">".get_string('usedropdown', 'languagelesson', $a)."</label>";
				echo "<input type=\"checkbox\" id=\"dropdown[$i]\" name=\"dropdown[$i]\" value=\"1\" />";
				echo "</td><td class=\"answerrow_cell\">";
				// print the score input
				echo '<b>'.get_string("score", "languagelesson")." $iplus1:</b><br />"
					."<input type=\"text\" name=\"score[$i]\" size=\"5\" "
					."value=\"".((isset($data->score[$i])) ? $data->score[$i] : $defaultpoints)."\" />";
				echo "</td></tr></table>";
			}

			// insert a visual break between answers and feedback areas
			echo '<tr><td></td></tr>';

			echo '<tr><td><table>';
			// print the correct feedback area
			echo "<tr><td class=\"answerrow_cell\"><b>".get_string("correctresponse", "languagelesson").":</b><br />\n";
			print_textarea(false, 1, 50, 0, 0, "correctresponse", ((isset($data->correctresponse) ? $data->correctresponse : '')));
			echo "<input type=\"hidden\" name=\"correctresponsescore\" value=\"1\" />";
			echo "</td><td class=\"answerrow_cell\">\n";
			// print the jump area
			echo "<b>".get_string("correctanswerjump", "languagelesson").":</b><br />\n";
			choose_from_menu($jump, "correctanswerjump", ((isset($data->jumpto[$i])) ? $data->jumpto[$i] : LL_NEXTPAGE), "");
			helpbutton("jumpto", get_string("jump", "languagelesson"), "languagelesson");
			echo "</td></tr></table>\n";

			echo '</td></tr>';

			$i++;

			echo '<tr><td><table>';
			// print the wrong feedback area
			echo "<tr><td class=\"answerrow_cell\"><b>".get_string("wrongresponse", "languagelesson").":</b><br />\n";
			print_textarea(false, 1, 50, 0, 0, "wrongresponse", ((isset($data->wrongresponse) ? $data->wrongresponse : '')));
			echo "<input type=\"hidden\" name=\"wrongresponsescore\" value=\"0\" />";
			
			echo "</td><td class=\"answerrow_cell\">\n";
			// print the jump area
			echo "<b>".get_string("wronganswerjump", "languagelesson").":</b><br />\n";
			choose_from_menu($jump, "wronganswerjump", ((isset($data->jumpto[$i])) ? $data->jumpto[$i] : 0), "");
			helpbutton("jumpto", get_string("jump", "languagelesson"), "languagelesson");

			echo "</td></tr></table>\n";

			echo '</tr></table>';

			break;

		default :
			break;
    }
    // close table and form
    ?>
    </table><br />
	<script type="text/javascript">
		var actionInput = document.getElementById('actioninput');
		function setAction(action) {
			actionInput.value = action;
		}
	</script>
	<?php if ($qtype != LL_TRUEFALSE
				&& $qtype != LL_AUDIO
				&& $qtype != LL_VIDEO
				&& $qtype != LL_ESSAY) { ?>
	<input type="submit" onclick="setAction('addpage');" value="<?php print_string('add4moreanswerfields', 'languagelesson'); ?>" />
	<?php } ?>
	<br /><br />
    <input type="submit" onclick="setAction('insertpage');" value="<?php print_string("savepage", "languagelesson") ?>" />
    <input type="submit" onclick="setAction('insertpage');" name="cancel" value="<?php print_string("cancel") ?>" />
    </fieldset>
    </form>
