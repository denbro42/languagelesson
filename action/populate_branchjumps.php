<?php
// now use this to generate a Javascript base array, chunks of which can then be selected to dynamically update later branching
// options as earlier branches are chosen
?>
<script type="text/javascript">

var basePages = new Array();

var pageid_to_index = new Array();
<?php
// print out the valid pages only if they exist
$i = 1;
if ($pages) { reset($pages); } // each($pages) requires that array counter be at the beginning
while ($pages && list($thispageid,$thispage) = each($pages)) {
        ?>
    nextpage = new Object();
    nextpage.pageid = <?php echo $thispageid; ?>;
    nextpage.title = '<?php echo $thispage->title; ?>';
    basePages[<?php echo $i; ?>] = nextpage;

    pageid_to_index[<?php echo $thispageid; ?>] = <?php echo $i; ?>;
        <?php
    $i++;
}
?>
default_page = new Object();
default_page.pageid = 0;
default_page.title = '--';
basePages.push(default_page);

function update_page_choices(selector, selector_number) {
    // get the updated list of valid page choices
    var new_valid_choices = get_choices_after(selector.value);
    
    // and now loop over all selectors after this one and apply the new_valid_choices to them
    var selectors_to_change = get_selectors_after(selector_number);

    var index = pageid_to_index[selector.value];
    for (i=0; i<selectors_to_change.length; i++) {
        this_selector = selectors_to_change[i];
        this_selector.options.length = 0;
        for (j=0; j<new_valid_choices.length; j++) {
            opt = new_valid_choices[j];
            this_selector.options[j] = new Option(opt.title, opt.pageid);
        }
        this_selector.options.selectedIndex = j-1;
    }
}

function get_choices_after(pageid) {
    if (pageid == 0) {
        return basePages.slice(basePages.length-1);
    }
    var index = pageid_to_index[pageid];
    if (index < basePages.length) {
        var new_choices = basePages.slice(index+1);
    }
    return new_choices;
}

function get_selectors_after(sel_num) {
    var i = sel_num+1;
    var ret_selectors = []
    // keep pulling jumpto[i]s until searching for jumpto[i] returns an empty array, at which point we've gone to the end of
    // the jumpto fields
    while ((next_selectors = document.getElementsByName('jumpto['+i+']')).length) {
        ret_selectors.push(next_selectors[0]);
        i++;
    }

    return ret_selectors;
}

</script>
<?php

$pageid = required_param('pageid', PARAM_INT);
$firstpage = optional_param('firstpage', 0, PARAM_INT);

// set of jump array
$jump = array();
// if there are pages after where this BT is getting created, populate the list with them
if (!$firstpage && $pages) {
    foreach ($pages as $apageid => $apage) {
        $jump[$apageid] = $apage->title;
    }
    // now, need to remove the first valid page, because the first branch is forced to jump to it, and no others can
    $firstjump = array_slice($jump, 0, 1, true);
    $jump = array_slice($jump, 1, null, true);
    // and add in the "end of lesson" default choice
    $jump[0] = '--';
// otherwise, just make all branches default to jumping to the end of the lesson
} else {
    $jump[0] = '--';
    $firstjump = $jump;
}

?>
