<?php // $Id: insertpage.php 677 2011-10-12 18:38:45Z griffisd $
/**
 * Action for processing the form from addpage action and inserts the page.
 *
 * @version $Id: insertpage.php 677 2011-10-12 18:38:45Z griffisd $
 * @license http://www.gnu.org/copyleft/gpl.html GNU Public License
 * @package languagelesson
 **/


require_once(dirname(__FILE__).'/../../../config.php');
require_once($CFG->dirroot.'/mod/languagelesson/locallib.php');


/**
 *
 *
 *
 * The $insertdata form has the following attributes:
 *   -> qtype                   :: The question type of the page to insert
 *   -> maxanswers
 *   -> title
 *   -> contents
 *   -> layout
 *   -> pageid
 *
 *   -> answer
 *   -> response
 *   -> jumpto
 *   -> score
 *   -> dropdown 
 *
 *   -> correctresponse
 *   -> correctresponsescore
 *   -> correctanswerjump
 *   -> wrongresponse
 *   -> wrongresponsescore
 *   -> wronganswerjump
 */
class languagelesson_page_inserter {


    public function insert($lesson, $insertdata, $timenow) {
        // if this page is a CLOZE, validate it before we change any records
        if ($insertdata->qtype == LL_CLOZE) {
            // note that since the text is coming from TinyMCE, it's pre-slashed, so we have to strip the slashes to get a proper
            // DOMDocument reading of the HTML
            languagelesson_validate_cloze_text($insertdata);
        }

        // if it's a branch table, make sure there were branches put in to save (otherwise, do NOT want to save the page record)
        if ($insertdata->qtype == LL_BRANCHTABLE) {
            $maxanswers = $insertdata->maxanswers;
            $hasbranches = 0;
            for ($i = 0; $i < $maxanswers; $i++) {
                if (! empty($insertdata->answer[$i])) { $hasbranches=true; break; }
            }
            if (! $hasbranches) {
                error('This branch table has no branches!');
            }
        }




    /////////////////////////////////////////////////
    // THE PAGE RECORD
    /////////////////////////////////////////////////

        // now build the newpage object
        $newpage = new stdClass;
        $newpage->lessonid = clean_param($lesson->id, PARAM_INT);
        $newpage->timecreated = $timenow;
        $newpage->qtype = clean_param($insertdata->qtype, PARAM_INT);
        $newpage->qoption = ((isset($insertdata->qoption)) ? clean_param($insertdata->qoption, PARAM_INT) : 0);
        $newpage->layout = ((isset($insertdata->layout)) ? clean_param($insertdata->layout, PARAM_INT) : 0);
        $newpage->title = addslashes(clean_param($insertdata->title, PARAM_CLEANHTML));
        $newpage->contents = addslashes(trim($insertdata->contents));

        // init the vars for storing data around the new page to false;
        $prevpage = false;
        $former_firstpage = false;
        // pull the previous page, if there is one
        if ($insertdata->pageid && !$prevpage = get_record('languagelesson_pages', 'id', $insertdata->pageid)) {
            error("Insert page: previous page record not found");
        // if no pageid was handed in, the new page is going first, so pull the former first page, if it exists
        // no error thrown here because a first page is not required (can be in an empty LL)
        } else if (! $insertdata->pageid) {
            $former_firstpage = get_record('languagelesson_pages', 'prevpageid', 0, 'lessonid', $newpage->lessonid);
        }

        // set the prevpageid
        if ($prevpage) {
            $prevpageid = $prevpage->id;
        } else {
            // this will be the new first page
            $prevpageid = 0;
        }
        $newpage->prevpageid = $prevpageid;

        // set the nextpageid
        // if newpage is not the new first page, pull the ID of the page with the next ordering value after the prevpage; ordering is
        // used instead of $prevpage->nextpageid because nextpageids do not always correspond to actual page order
        if ($prevpage) {
            // if there is no page after this, then newpage is the last page
            if (! $nextpageid = get_field('languagelesson_pages', 'id', 'lessonid', $lesson->id, 'ordering', $prevpage->ordering + 1)) {
                $nextpageid = 0;
            }
        // if so, then if there was already a firstpage, just use its ID
        } else if ($former_firstpage) {
            $nextpageid = $former_firstpage->id;
        // otherwise, this is the last page, too
        } else {
            $nextpageid = 0;
        }
        $newpage->nextpageid = $nextpageid;

        // set the ordering
        if ($prevpage) {
            $ordering = $prevpage->ordering + 1;
        } else {
            $ordering = 1;
        }
        $newpage->ordering = $ordering;

        // set the branchid
        // the new page only gets a branch id if it's not the first page (first page can't be in a branch)
        if ($prevpage) {
            // if the previous page is a branch table, this goes in the first branch
            if ($prevpage->qtype == LL_BRANCHTABLE) {
                $branchid = get_field('languagelesson_branches', 'id', 'parentid', $prevpage->id, 'ordering', 1);
            // if the preceding page is an end of branch...
            } else if ($prevpage->qtype == LL_ENDOFBRANCH) {
                if ($newpage->nextpageid) {
                    $branchid = get_field('languagelesson_pages', 'branchid', 'id', $newpage->nextpageid);
                // if there is no following page, it gets the same branchid as the parent BT
                } else {
                    $parentBT = get_field('languagelesson_branches', 'parentid', 'id', $prevpage->branchid);
                    $branchid = get_field('languagelesson_pages', 'branchid', 'id', $parentBT);
                }
            // otherwise, it's in the same branch as the preceding page
            } else {
                $branchid = $prevpage->branchid;
            }
            $newpage->branchid = $branchid;
        }

        // insert the page record
        $newpage->id = insert_record("languagelesson_pages", $newpage);
        if (!$newpage->id) {
            error("Insert page: new page not inserted");
        }

        // update the prevpageid and nextpageid values around the new page
        if ($newpage->prevpageid
                && (get_field('languagelesson_pages','qtype','id',$newpage->prevpageid) != LL_ENDOFBRANCH
                    || languagelesson_is_last_branch_end($newpage->prevpageid))
                && !set_field("languagelesson_pages", "nextpageid", $newpage->id, "id", $newpage->prevpageid)) {
            error("Insert page: unable to update next link");
        }
        //if ($newpage->prevpageid
        //        && !set_field("languagelesson_pages", "nextpageid", $newpage->id, "id", $newpage->prevpageid)) {
        //    error("Insert page: unable to update next link");
        //}
        if ($prevpage && $newpage->nextpageid && !set_field("languagelesson_pages", "prevpageid", $newpage->id, "id",
                    $newpage->nextpageid)) {
            error("Insert page: unable to update previous link");
        } else if (!$prevpage && $former_firstpage
                        && ! set_field_select('languagelesson_pages', 'prevpageid', $newpage->id, "prevpageid = 0
                                                                                                 AND id != $newpage->id")) {
            error("Insert page: unable to mark the former first page as after new page");
        }

        // update the ordering values of pages after this one
        if ($upages = get_records_select('languagelesson_pages', "lessonid=$lesson->id and ordering >= $newpage->ordering and id !=
                    $newpage->id")) {
            foreach ($upages as $upage) {
                set_field('languagelesson_pages', 'ordering', $upage->ordering + 1, 'id', $upage->id);
            }
        }

        // if the page was inserted at the start of a branch table, update the firstpage pointer of the first branch
        if ($prevpage && ($prevpage->qtype == LL_BRANCHTABLE
                          || $prevpage->qtype == LL_ENDOFBRANCH) && !languagelesson_is_last_branch_end($prevpage)) {
            set_field('languagelesson_branches', 'firstpage', $newpage->id, 'id', $newpage->branchid);
        }


    /////////////////////////////////////////////////
    /////////////////////////////////////////////////




    /////////////////////////////////////////////////
    // ANSWERS (IF APPLICABLE)
    /////////////////////////////////////////////////
        if ($insertdata->qtype != LL_BRANCHTABLE) {
            if ($insertdata->qtype == LL_ESSAY || $insertdata->qtype == LL_AUDIO || $insertdata->qtype == LL_VIDEO) {
                $newanswer = new stdClass;
                $newanswer->lessonid = $lesson->id;
                $newanswer->pageid = $newpage->id;
                $newanswer->timecreated = $timenow;
                if (isset($insertdata->jumpto[0])) { $newanswer->jumpto = clean_param($insertdata->jumpto[0], PARAM_INT); }
                if (isset($insertdata->score[0])) { $newanswer->score = clean_param($insertdata->score[0], PARAM_NUMBER); }
                $newanswerid = insert_record("languagelesson_answers", $newanswer);
                if (!$newanswerid) {
                    error("Insert Page: answer record not inserted");
                }
            } else {
                $maxanswers = $insertdata->maxanswers;
                if ($insertdata->qtype == LL_MATCHING) {
                    // need to add two to offset correct response and wrong response
                    $maxanswers = $maxanswers + 2;
                }
                for ($i = 0; $i < $maxanswers; $i++) {
                    // re-initialize to a blank answer object
                    $newanswer = new stdClass();
                    $newanswer->lessonid = $lesson->id;
                    $newanswer->pageid = $newpage->id;
                    $newanswer->timecreated = $timenow;
                    if (!empty($insertdata->answer[$i]) and trim(strip_tags($insertdata->answer[$i]))) { //strip_tags because the
                                                                                                         //HTML editor adds <p><br />
                        $newanswer->answer = trim($insertdata->answer[$i]);
                        // if this is a CLOZE, need a hard-coded way to distinguish ordering of questions, so note that here
                        if ($insertdata->qtype == LL_CLOZE) { $newanswer->answer = $i.'|'.$newanswer->answer; }
                        if (isset($insertdata->response[$i])) { $newanswer->response = trim($insertdata->response[$i]); }
                        if (isset($insertdata->jumpto[$i])) { $newanswer->jumpto = clean_param($insertdata->jumpto[$i], PARAM_INT);    }
                        if (isset($insertdata->score[$i])) { $newanswer->score = clean_param($insertdata->score[$i], PARAM_NUMBER);    }
                        // if this is a cloze subquestion marked as a dropdown, save it as such
                        if ($insertdata->qtype == LL_CLOZE && isset($insertdata->dropdown[$i])) {
                            $newanswer->flags = 1;
                        }
                    } else if ($insertdata->qtype != LL_MATCHING) {
                        break;
                    }
                    $newanswerid = insert_record("languagelesson_answers", $newanswer);
                    if (!$newanswerid) {
                        error("Insert Page: answer record $i not inserted");
                    }
                }

                // if this is a CLOZE type, then the responses are not associated with specific answers, so save them here on their own
                if ($insertdata->qtype == LL_CLOZE) {
                    // initialize the answer template to save the responses with
                    $newanswer = new stdClass();
                    $newanswer->lessonid = $lesson->id;
                    $newanswer->pageid = $newpage->id;
                    $newanswer->timecreated = $timenow;

                    // set the responses
                    if (isset($insertdata->correctresponse)) {
                        $newanswer->response = trim($insertdata->correctresponse);
                        $newanswer->score = $insertdata->correctresponsescore;
                        $newanswer->jumpto = clean_param($insertdata->correctanswerjump, PARAM_INT);
                        if (!$newanswerid = insert_record('languagelesson_answers', $newanswer)) {
                            error("Insert page: correct response not inserted");
                        }
                    }

                    if (isset($insertdata->wrongresponse)) {
                        $newanswer->response = trim($insertdata->wrongresponse);
                        $newanswer->score = $insertdata->wrongresponsescore;
                        $newanswer->jumpto = clean_param($insertdata->wronganswerjump, PARAM_INT);
                        if (!$newanswerid = insert_record('languagelesson_answers', $newanswer)) {
                            error("Insert page: wrong response not inserted");
                        }
                    }
                }
            }
        

            // Now that setting answers is done, update the languagelesson instance's calculated max grade
            languagelesson_recalculate_maxgrade($lesson->id);

        }

    /////////////////////////////////////////////////
    /////////////////////////////////////////////////




    /////////////////////////////////////////////////
    // BRANCHES (IF APPLICABLE)
    /////////////////////////////////////////////////


        // if we just inserted a branch table, handle creating branch records and ENDOFBRANCH page records here
        else {
            $maxanswers = $insertdata->maxanswers;
            
            //init array to hold $branch objects for use in ENDOFBRANCH page population
            $branches = array();

            // create languagelesson_branch records
            //   - one for each branch
            //   - stores the pageid of the first page in the branch (that is, the one specified as the branch's jumpto in the submitted
            //   data
            //   - if submitted data just has the jumpto as NEXTPAGE (default setting), firstpage points to 0
            $ordering = 0;
            for ($i = 0; $i < $maxanswers; $i++) {
                // since maxanswers is the number of maximum POSSIBLE answers, some of these may be empty; if so, skip them
                if (empty($insertdata->answer[$i])) { continue; }

                $branch = new stdClass;
                $branch->lessonid = $lesson->id;
                $branch->parentid = $newpage->id;
                $branch->ordering = ++$ordering;
                $branch->title = addslashes(trim($insertdata->answer[$i]));
                $branch->timecreated = time();
                // set the firstpage field (a jumpto value of 0 means stick it at the end of the lesson)
                $branch->firstpage = $insertdata->jumpto[$i];

                if (! insert_record('languagelesson_branches', $branch)) {
                    error('Insert page: branch record not inserted');
                }
            }

            // now pull the just-created branches in order to use their IDs
            $branches = get_records('languagelesson_branches', 'parentid', $newpage->id, 'ordering');
            // get rid of the records being keyed to their ids (give them sequential, meaningless indices)
            $branches = array_values($branches);


            // determine what the nextpageid should be for the case of inserting an EOB at the end of the current level (lesson or branch)
            // - if the parent branch table has no branch ID, this branching structure is not inside another branching structure, so the
            // EOB is being inserted at lesson level, therefore the nextpageid will be 0 (marking it as the end of the lesson)
            // - if the parent branch table does have a branch ID, the EOB is being inserted at branch level, so its nextpageid should be
            // the id of the EOB record ending the containing branch
            if (!isset($newpage->branchid) || !$newpage->branchid) {
                $endpageid = 0;
            } else {
                $containingBranchPages = get_records('languagelesson_pages', 'branchid', $newpage->branchid, 'ordering');
                $lastContainingBranchPage = end($containingBranchPages);
                $endpageid = $lastContainingBranchPage->id;
            }


            // create the ENDOFBRANCH page records
            //   - for all except last one, nextpageid points to the parent branch table
            //   - use placement of branch head n+1 to decide where EOB n goes
            for ($i=0; $i<count($branches); $i++) {
                $branch = $branches[$i];

                $neweob = new stdClass;
                $neweob->lessonid = $lesson->id;
                $neweob->branchid = $branch->id;
                $neweob->qtype = LL_ENDOFBRANCH;
                $neweob->timecreated = time();
                $neweob->title = 'ENDOFBRANCH';

                // determine prevpageid as follows:
                // - if this is the last branch, the EOB becomes the last page in the current structural level (e.g. lesson or
                // branch), period, so its prevpageid is set to the ID of what is currently the last page in that level
                // - if the next branch record has a firstpageid, this EOB's prevpageid becomes that page's prevpageid
                // - if the next branch does not have a firstpageid, this EOB becomes the last page in the current level
                $goesAtEnd = false;
                if ($i+1 < count($branches) && $branches[$i+1]->firstpage) {
                    $neweob->prevpageid = get_field('languagelesson_pages', 'id', 'nextpageid', $branches[$i+1]->firstpage);
                } else {
                    $neweob->prevpageid = get_field('languagelesson_pages', 'id', 'nextpageid', $endpageid, 'lessonid', $lesson->id);
                    $goesAtEnd = true;
                }

                // set nextpageid; if this is being inserted as the last page in the current structural level, its nextpageid will
                // be that of what was formerly the last page in the same level (e.g. 0 if inserted at lesson level, the parent BT
                // or next page after the complete branchset if inserted at branch level)
                if ($goesAtEnd) {
                    $neweob->nextpageid = $endpageid;
                } else {
                    $neweob->nextpageid = $newpage->id;
                }

                // insert the EOB page
                if (! $neweobid = insert_record('languagelesson_pages', $neweob)) {
                    error('Insert page: failed to insert EndOfBranch record');
                }

                // and now that we have the ID, go back and correct the references of the pages around the new EOB

                // handle nextpageid of the page preceding the EOB record
                // - if the page directly before the new EOB is not itself an EOB, point it to the EOB as next page
                if (get_field('languagelesson_pages', 'qtype', 'id', $neweob->prevpageid) != LL_ENDOFBRANCH) {
                    set_field('languagelesson_pages', 'nextpageid', $neweobid, 'id', $neweob->prevpageid);
                // - if it is an EOB, however, point it to the branch table, as its old nextpageid value should currently be 0 (it
                // was the last page in the LL instance)
                } else {
                    set_field('languagelesson_pages', 'nextpageid', $branch->parentid, 'id', $neweob->prevpageid);
                }

                // handle prevpageid of the page following the EOB record
                // - if the branch following this EOB has a firstpage pointer (that is, it has content), then point that following
                // branch's first page's prevpageid value to the newly-inserted EOB record
                if ($i+1 < count($branches) && $branches[$i+1]->firstpage) {
                    set_field('languagelesson_pages', 'prevpageid', $neweobid, 'id', $branches[$i+1]->firstpage);
                // - if this is the last EOB record; if it's not getting inserted inside another branch level, there will be no page after
                // this, so don't bother setting anything, but if this is inside another branch structure, then set that containing
                // branch's EOB to be following this last record
                } else if ($i+1 == count($branches) && $endpageid) {
                    set_field('languagelesson_pages', 'prevpageid', $neweobid, 'id', $endpageid);
                }

            }


            // update the languagelesson instance's ordering values, for certainty of accuracy
            languagelesson_update_ordering($lesson->id);

        }


    /////////////////////////////////////////////////
    /////////////////////////////////////////////////

        return $newpage;

    }

}
?>
