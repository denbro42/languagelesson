<?php // $Id: move.php 677 2011-10-12 18:38:45Z griffisd $
/**
 * Action for actually moving the page (database changes)
 *
 * @version $Id: move.php 677 2011-10-12 18:38:45Z griffisd $
 * @license http://www.gnu.org/copyleft/gpl.html GNU Public License
 * @package lesson
 **/
    
// pull in the moving library
require('move_class.php');

$movepageid = required_param('pageid', PARAM_INT); //  page to move
if (!$movepage = get_record("languagelesson_pages", "id", $movepageid)) {
    error("Move: page not found");
}
$mode = required_param('mode', PARAM_RAW);
if ($mode == 'showslots') {
    $display = new languagelesson_page_mover();
    $display->cmid = $cm->id;
    $display->lessonid = $lesson->id;
    $display->displaySlots($movepage);

} else if ($mode == 'move') {
    require_capability('mod/languagelesson:edit', $context);
    require_sesskey();

    $after = required_param('after', PARAM_INT); // target page

    $mover = new languagelesson_page_mover();
    $mover->lessonid = $lesson->id;
    $mover->move($movepage, $after);

    languagelesson_set_message(get_string('movedpage', 'languagelesson'), 'notifysuccess');
    redirect("$CFG->wwwroot/mod/languagelesson/edit.php?id=$cm->id");
}

?>
