<?PHP // $Id: languagelesson.php 677 2011-10-12 18:38:45Z griffisd $ 
      // language_lesson.php - created with Moodle 1.9.9 beta + (2010082600)

$string['accesscontrol'] = 'Access control';
$string['actionaftercorrectanswer'] = 'Action after correct answer';
$string['actions'] = 'Actions';
$string['activitylink'] = 'Link to an activity';
$string['activitylinkname'] = 'Go to: $a';
$string['addabranchtable'] = 'Add a Branch Table';
$string['addanendofbranch'] = 'Add an End of Branch';
$string['addaquestionpage'] = 'Add a Question Page';
$string['addaquestionpagehere'] = 'Add a question page here';
$string['addcluster'] = 'Add a Cluster';
$string['addedabranchtable'] = 'Added a Branch Table';
$string['addedanendofbranch'] = 'Added an End of Branch';
$string['addedaquestionpage'] = 'Added a Question Page';
$string['addedcluster'] = 'Added a Cluster';
$string['addedendofcluster'] = 'Added an End of Cluster';
$string['addendofcluster'] = 'Add an End of Cluster';
$string['addpage'] = 'Add a page';
$string['anchortitle'] = 'Start of main content';
$string['and'] = 'AND';
$string['answer'] = 'Answer';
$string['answeredcorrectly'] = 'answered correctly.';
$string['answersfornumerical'] = 'Answers for Numerical questions should be matched pairs of Minimum and Maximum values';
$string['arrangebuttonshorizontally'] = 'Arrange Branch buttons horizontally?';
$string['attempts'] = 'Attempts';
$string['attemptsdeleted'] = 'Deleted attempts';
$string['attemptsremaining'] = 'You have $a attempt(s) remaining';
$string['available'] = 'Available from';
$string['averagescore'] = 'Average score';
$string['averagetime'] = 'Average time';
$string['branchtable'] = 'Branch Table';
$string['cancel'] = 'Cancel';
$string['canretake'] = '$a can re-take';
$string['useregex'] = 'Use Regular Expressions';
$string['checkbranchtable'] = 'Check branch table';
$string['checkedthisone'] = 'checked this one.';
$string['checknavigation'] = 'Check navigation';
$string['checkquestion'] = 'Check question';
$string['classstats'] = 'Class statistics';
$string['clicktodownload'] = 'Click on the following link to download the file.';
$string['clicktopost'] = 'Click here to post your grade on the High Scores list.';
$string['clusterjump'] = 'Unseen question within a cluster';
$string['clustertitle'] = 'Cluster';
$string['collapsed'] = 'Collapsed';
$string['comments'] = 'Comments';
$string['completed'] = 'Completed';
$string['completederror'] = 'Complete the lesson';
$string['completethefollowingconditions'] = 'You must complete the following condition(s) in <b>$a</b> lesson before you can proceed.';
$string['conditionsfordependency'] = 'Condition(s) for the dependency';
$string['confirmdeletionofthispage'] = 'Confirm deletion of this page';
//$string['congratulations'] = 'Congratulations - end of lesson reached';
$string['congratulations'] = 'Congratulations - lesson completed';
$string['continue'] = 'Continue';
$string['continuetoanswer'] = 'Continue to change answers.';
$string['correctanswerjump'] = 'Correct answer jump';
$string['correctanswerscore'] = 'Correct answer score';
$string['correctresponse'] = 'Correct answer feedback';
$string['credit'] = 'Credit';
$string['deadline'] = 'Deadline';
$string['defaultessayresponse'] = 'Your essay will be graded by the course instructor.';
$string['deletedefaults'] = 'Deleted $a x languagelesson default';
$string['deletedpage'] = 'Deleted page';
$string['deleting'] = 'Deleting';
$string['deletingpage'] = 'Deleting page: $a';
$string['dependencyon'] = 'Dependent on';
$string['detailedstats'] = 'Detailed statistics';
$string['didnotanswerquestion'] = 'Did not answer this question.';
$string['didnotreceivecredit'] = 'Did not receive credit';
$string['displaydefaultfeedback'] = 'Display default feedback';
$string['defaultcorrectfeedback'] = 'Default correct feedback';
$string['defaultcorrectfeedbacktext'] = 'Well done!';
$string['defaultwrongfeedback'] = 'Default incorrect feedback';
$string['defaultwrongfeedbacktext'] = 'That\'s the wrong answer.';
$string['displayinleftmenu'] = 'Display in left menu?';
$string['displayleftif'] = 'Display left menu only if grade greater than:';
$string['displayleftmenu'] = 'Display left menu';
$string['displayofgrade'] = 'Display of grade (for students only)';
$string['edit'] = 'Edit';
$string['editlessonsettings'] = 'Edit language lesson settings';
$string['editpagecontent'] = 'Edit page contents';
$string['email'] = 'Email';
$string['emailallgradedessays'] = 'Email ALL<br />graded essays';
$string['emailgradedessays'] = 'Email graded essays';
$string['emailsuccess'] = 'Emails sent successfully';
$string['endofbranch'] = 'End of branch';
$string['endofclustertitle'] = 'End of cluster';
$string['endoflesson'] = 'End of language lesson';
$string['enteredthis'] = 'entered this.';
$string['entername'] = 'Enter a nickname for the high scores list';
$string['eolstudentoutoftime'] = 'Attention:  You ran out of time for this lesson.  Your last answer may not have counted if it was answered after the time was up.';
$string['eolstudentoutoftimenoanswers'] = 'You did not answer any questions.  You have received a 0 for this lesson.';
$string['essayemailmessage'] = '<p>Essay prompt:<blockquote>$a->question</blockquote></p><p>Your response:<blockquote><em>$a->response</em></blockquote></p><p>$a->teacher\'s comments:<blockquote><em>$a->comment</em></blockquote></p><p>You have received $a->earned out of $a->outof for this essay question.</p><p>Your grade for the lesson has been changed to $a->newgrade&#37;.</p>';
$string['essayemailsubject'] = 'Your grade for $a question';
$string['essays'] = 'Essays';
$string['essayscore'] = 'Essay score';
$string['fileformat'] = 'File Format';
$string['firstanswershould'] = 'First answer should jump to the \"Correct\" Page';
$string['firstwrong'] = 'Unfortunately you cannot earn this one point, because your response was not correct.  Would you like to keep guessing, just for the sheer joy of learning (but for no point credit)?';
$string['flowcontrol'] = 'Flow control';
$string['full'] = 'Expanded';
$string['general'] = 'General';
$string['grade'] = 'Grade';
$string['gradebetterthan'] = 'Grade better than (&#37;)';
$string['gradebetterthanerror'] = 'Earn a grade better than $a percent';
$string['gradeessay'] = 'Grade essay questions ($a->notgradedcount not graded and $a->notsentcount not sent)';
$string['gradeis'] = 'Grade is $a';
$string['gradeoptions'] = 'Grade options';
$string['handlingofretakes'] = 'Handling of re-takes';
$string['havenotgradedyet'] = 'Have not graded yet.';
$string['here'] = 'here';
$string['hightime'] = 'High time';
$string['importcount'] = 'Importing $a questions';
$string['importppt'] = 'Import PowerPoint';
$string['importquestions'] = 'Import questions';
$string['insertedpage'] = 'Inserted page';
$string['jump'] = 'Jump';
$string['jumps'] = 'Jumps';
$string['jumpsto'] = 'Jumps to <em>$a</em>';
$string['leftduringtimed'] = 'You have left during a timed lesson.<br />Please click on Continue to restart the lesson.';
$string['leftduringtimednoretake'] = 'You have left during a timed lesson and you are<br />not allowed to retake or continue the lesson.';
$string['languagelesson:edit'] = 'Edit a language lesson activity';
$string['languagelesson:manage'] = 'Manage a language lesson activity';
$string['languagelesson:grade'] = 'Grade a language lesson activity';
$string['lessonattempted'] = 'Lesson attempted';
$string['lessonclosed'] = 'This lesson closed on $a.';
$string['lessoncloses'] = 'Lesson closes';
$string['lessoncloseson'] = 'Lesson closes on $a';
$string['lessondefault'] = 'Use this lesson\'s settings as defaults';
$string['lessonformating'] = 'Lesson formatting';
$string['lessonmenu'] = 'Lesson menu';
$string['lessonnotready'] = 'This lesson is not ready to be taken.  Please contact your $a.';
$string['lessonopen'] = 'This lesson will be open on $a.';
$string['lessonopens'] = 'Lesson opens';
$string['lessonpagelinkingbroken'] = 'First page not found.  Lesson page linking must be broken.  Please contact an admin.';
$string['lessonstats'] = 'Lesson statistics';
$string['linkedmedia'] = 'Linked media';
$string['loginfail'] = 'Login failed, please try again...';
$string['lowscore'] = 'Low score';
$string['lowtime'] = 'Low time';
$string['manualgrading'] = 'Grade Essays';
$string['matchesanswer'] = 'Matches with answer';
$string['maximumnumberofanswersbranches'] = 'Maximum number of answers/branches';
$string['maximumnumberofattempts'] = 'Maximum number of attempts';
$string['maximumnumberofattemptsreached'] = 'Maximum number of attempts reached - Moving to next page';
$string['maxtime'] = 'Time limit (minutes)';
$string['maxtimewarning'] = 'You have $a minute(s) to finish the lesson.';
$string['mediafile'] = 'Pop-up to file or web page';
$string['mediafilepopup'] = 'Click here to view';
$string['mediaheight'] = 'Window height:';
$string['mediawidth'] = 'width:';
$string['minimumnumberofquestions'] = 'Minimum number of questions';
$string['missingname'] = 'Please enter a nickname';
$string['modulename'] = 'Language Lesson';
$string['modulenameplural'] = 'Language Lessons';
$string['movedpage'] = 'Moved page';
$string['movepagehere'] = 'Move page to here';
$string['moving'] = 'Moving page: $a';
$string['multianswer'] = 'Multianswer';
$string['multipleanswer'] = 'Multiple Answer';
$string['nameapproved'] = 'Name approved';
$string['namereject'] = 'Sorry, your name has been rejected by the filter.<br />Please try another name.';
$string['nextpage'] = 'Next page';
$string['noanswer'] = 'No answer given.  Please submit an answer.';
$string['noattemptrecordsfound'] = 'No attempt records found: no grade given';
$string['nobranchtablefound'] = 'No Branch Table found';
$string['nocommentyet'] = 'No comment yet.';
$string['nocoursemods'] = 'No activities found';
$string['nocredit'] = 'No credit';
$string['nodeadline'] = 'No deadline';
$string['noessayquestionsfound'] = 'No essay questions found in this lesson.';
$string['nolessonattempts'] = 'No attempts have been made on this lesson.';
$string['nooneansweredcorrectly'] = 'No one answered correctly.';
$string['nooneansweredthisquestion'] = 'No one answered this question.';
$string['noonecheckedthis'] = 'No one checked this.';
$string['nooneenteredthis'] = 'No one entered this.';
$string['noonehasanswered'] = 'No one has answered an essay question yet.';
$string['noretake'] = 'You are not allowed to retake this lesson.';
$string['normal'] = 'Normal - follow lesson path';
$string['notcompleted'] = 'Not completed';
$string['notdefined'] = 'Not defined';
$string['notitle'] = 'No title';
$string['numberofcorrectanswers'] = 'Number of correct answers: $a';
$string['numberofcorrectmatches'] = 'Number of correct matches: $a';
$string['numberofpagesviewed'] = 'Number of questions answered: $a';
$string['showongoingscore'] = 'Display ongoing score';
$string['ongoingscoremessage'] = 'You have earned $a->score point(s) out of $a->currenthigh point(s) thus far.';
$string['or'] = 'OR';
$string['ordered'] = 'Ordered';
$string['other'] = 'Other';
$string['outof'] = 'Out of $a';
$string['overview'] = 'Overview';
$string['page'] = 'Page: $a';
$string['pagecontents'] = 'Page contents';
$string['pages'] = 'Pages';
$string['pagetitle'] = 'Page title';
$string['pointsearned'] = 'Points earned';
$string['postsuccess'] = 'Post successful';
$string['preview'] = 'Preview';
$string['previewlesson'] = 'Preview $a';
$string['previouspage'] = 'Previous page';
$string['progressbar'] = 'Progress Bar';
$string['progressbarteacherwarning'] = 'Progress Bar does not display for $a';
$string['deleteallattempts'] = 'Delete all language lesson attempts';
$string['qtype'] = 'Page type';
$string['question'] = 'Question';
$string['questionoption'] = 'Question';
$string['questiontype'] = 'Question type';
$string['randombranch'] = 'Random branch page';
$string['randompageinbranch'] = 'Random question within a branch';
$string['rank'] = 'Rank';
$string['rawgrade'] = 'Raw grade';
$string['receivedcredit'] = 'Received credit';
$string['redisplaypage'] = 'Redisplay page';
$string['report'] = 'Report';
$string['reports'] = 'Reports';
$string['response'] = 'Feedback';
$string['returnto'] = 'Return to $a';
$string['returntocourse'] = 'Return to the course';
$string['sanitycheckfailed'] = 'Sanity check failed: This attempt has been deleted';
$string['savechanges'] = 'Save Changes';
$string['savechangesandeol'] = 'Save all changes and go to the end of the lesson.';
$string['savepage'] = 'Save page';
$string['savebranchtable'] = 'Save branch table';
$string['score'] = 'Score';
$string['scores'] = 'Scores';
$string['secondpluswrong'] = 'Not quite.  Would you like to try again?';
$string['showanunansweredpage'] = 'Show an unanswered Page';
$string['showanunseenpage'] = 'Show an Unseen Page';
$string['singleanswer'] = 'Single Answer';
$string['skip'] = 'Skip navigation';
$string['startlesson'] = 'Start lesson';
$string['studentattemptlesson'] = '$a->lastname, $a->firstname\'s attempt number $a->attempt';
$string['studentname'] = '$a Name';
$string['studentoneminwarning'] = 'Warning: You have 1 minute or less to finish the lesson.';
$string['studentresponse'] = '{$a}\'s response';
$string['submitname'] = 'Submit name';
$string['teacherjumpwarning'] = 'An $a->cluster jump or an $a->unseen jump is being used in this lesson.  The Next Page jump will be used instead.  Login as a student to test these jumps.';
$string['teacherongoingwarning'] = 'Ongoing score is only displayed for student.  Login as a student to test ongoing score';
$string['teachertimerwarning'] = 'Timer only works for students.  Test the timer by logging in as a student.';
$string['thatsthecorrectanswer'] = 'That\'s the correct answer';
$string['thatsthewronganswer'] = 'That\'s the wrong answer';
$string['thefollowingpagesjumptothispage'] = 'The following pages jump to this page';
$string['thispage'] = 'This page';
$string['timed'] = 'Timed';
$string['timeremaining'] = 'Time remaining';
$string['timespenterror'] = 'Spend at least $a minutes in the lesson';
$string['timespentminutes'] = 'Time Spent (minutes)';
$string['timetaken'] = 'Time taken';
$string['topscorestitle'] = 'Top $a High Scores';
$string['unseenpageinbranch'] = 'Unseen question within a branch';
$string['unsupportedqtype'] = 'Unsupported question type ($a)!';
$string['updatedpage'] = 'Updated page';
$string['updatefailed'] = 'Update failed';
$string['useeditor'] = 'Use editor';
$string['usemaximum'] = 'Use maximum';
$string['usemean'] = 'Use mean';
$string['viewgrades'] = 'View grades';



$string['viewreports'] = 'View $a->attempts completed $a->student attempts';
$string['welldone'] = 'Well done!';
$string['whatdofirst'] = 'What would you like to do first?';
$string['wronganswerjump'] = 'Wrong answer jump';
$string['wronganswerscore'] = 'Wrong answer score';
$string['wrongresponse'] = 'Wrong answer feedback';
$string['xattempts'] = '$a attempts';
$string['youhaveseen'] = 'You have seen more than one page of this lesson already.<br />Do you want to start at the last page you saw?';
$string['youranswer'] = 'Your answer';
$string['yourcurrentgradeis'] = 'Your current grade is $a';
$string['yourcurrentgradeisoutof'] = 'Your current grade is $a->grade out of $a->total';
$string['youshouldview'] = 'You should answer at least: $a';


$string['displayscorewithoutmanuals'] = 'Your score is $a->earned (out of $a->total).';
$string['displayscorewithmanuals'] = 'You earned $a->earned out of $a->tempmaxgrade for the automatically graded questions.<br />Your $a->nmanquestions essay/audio/video question(s) will be graded and added<br />into your final score at a later date.<br /><br />Your current grade without the essay/audio/video question(s) is $a->earned out of $a->total.';

$string['holisticgrader'] = 'Grading';

$string['languagelesson:submit'] = 'Submit a multimedia file in a language lesson activity';

$string['graderstudentcolumnname'] = 'Student';

///cell classes for grader
$string['graderautocorrect'] = 'autocorrect';
$string['graderautowrong'] = 'autowrong';
$string['gradernone'] = 'no_submission';
$string['gradernew'] = 'new';
$string['graderviewed'] = 'viewed';
$string['gradergraded'] = 'graded';
$string['gradercommented'] = 'commented';
$string['graderresubmit'] = 'resubmit';

///cell class descriptors for grader legend
$string['legendautocorrect'] = 'Correct';
$string['legendautowrong'] = 'Incorrect';
$string['legendnone'] = 'Unattempted question';
$string['legendnew'] = 'New entry';
$string['legendviewed'] = 'Viewed entry';
$string['legendcommented'] = 'Commented entry';
$string['legendresubmit'] = 'Corrected entry';

///cell contents by type for grader
$string['cellcontents_multiple_choice'] = 'MC';
$string['cellcontents_true_false'] = 'TF';
$string['cellcontents_short_answer'] = 'SA';
$string['cellcontents_match'] = 'Ma';
$string['cellcontents_essay'] = 'Es';
$string['cellcontents_audio'] = 'Au';
$string['cellcontents_video'] = 'Vi';

$string['legendshowquestionnamebox'] = 'Always show question name on mouseover';
$string['legendselectallstudentsbox'] = 'Select/Deselect all students';
$string['legenduseHTMLbox'] = 'Use HTML in notification emails';

$string['gradersavegrade'] = 'Save Feedback';

$string['maxnumretakes'] = 'Maximum number of re-takes';

$string['retakesmaxed'] = 'You have retaken this lesson the maximum number of times.';

$string['eolreachedincomplete'] = 'You have not yet completed this lesson.<br />You will not receive a grade until all questions have been answered.';

$string['eolreachedincompletereturntolesson'] = 'Return to Lesson';

$string['displayleftmenucontextcolor'] = 'Context colors in left menu';

$string['emailsubject'] = 'Feedback posted for $a->coursename:  $a->modulename:  $a->llname';
$string['emailmessage'] = '$a->teacher has posted feedback for your submissions in $a->llname.

You can see it appended to your submissions in:

$a->url';
$string['emailmessagehtml'] = '$a->teacher has posted feedback for your submissions in \'<i>$a->llname</i>\'<br /><br />You can see it appended to your submissions in <a href=\"$a->url\">$a->llname</a>.';

$string['automaticgrading'] = "Automatic grading";

$string['assigngradecolumnheader'] = 'Grade<br />(0-$a)';
$string['assigngradesbutton'] = "Assign grades";
$string['savedgradecolumnheader'] = "Saved grade";
$string['improperholisticgrade'] = "Please enter a grade between 0 and 100.";


$string['lastattempt'] = "This is your last attempt on this question!";
$string['reviewingtest'] = "You are currently viewing a completed language lesson. No changes will be recorded.";
$string['nomoreattempts'] = "You have already attempted this question the maximum number of times. No changes will be recorded.";

$string['type'] = 'Lesson type';
$string['practicetype'] = 'Practice';
$string['assignmenttype'] = 'Assignment';
$string['testtype'] = 'Test';

$string['usepenalty'] = 'Use penalty';
$string['penaltytype'] = 'Penalty type';
$string['penaltymean'] = 'Use mean';
$string['penaltyset'] = "Subtract set penalty";
$string['penaltyvalue'] = 'Penalty value (%%)';

$string['showoldanswer'] = 'Display previous answer';

$string['shuffleanswers'] = 'Shuffle answers';

$string['oldgradeheader'] = 'You have already completed this Language Lesson';
$string['oldgradeincomplete'] = 'You have not yet completed this Language Lesson';
$string['oldgradethisisyourgrade'] = 'Your current score on this language lesson is $a->grade/$a->maxgrade.';
$string['oldgradehasungraded'] = 'One or more of your submissions on this language lesson has not yet been graded. The above grade may
or may not reflect your final grade.';
$string['oldgradeassignmentmessage'] = 'If you would like to change your answers, please click the \'Enter lesson\' button below.';
$string['oldgradeassignmentbutton'] = 'Enter lesson';
$string['oldgradetestmessage'] = 'You are not allowed to change any of your answers, but if you would like to review them, please click the \'Review\' button below.';
$string['oldgradetestbutton'] = 'Review';
$string['oldgradeyouhavefeedback'] = 'Feedback has been posted on some of your submissions to this LanguageLesson.<br />To see the feedback, click on any of the following page links.<br />';


$string['descriptiontextid'] = 'LL_DESCRIPTION';
$string['descriptionname'] = 'Description';
$string['multichoicetextid'] = 'LL_MULTICHOICE';
$string['multichoicename'] = 'Multiple Choice';
$string['truefalsetextid'] = 'LL_TRUEFALSE';
$string['truefalsename'] = 'True/False';
$string['shortanswertextid'] = 'LL_SHORTANSWER';
$string['shortanswername'] = 'Short Answer';
$string['clozetextid'] = 'LL_CLOZE';
$string['clozename'] = 'Cloze';
$string['matchingtextid'] = 'LL_MATCHING';
$string['matchingname'] = 'Matching';
$string['numericaltextid'] = 'LL_NUMERICAL';
$string['numericalname'] = 'Numerical';
$string['essaytextid'] = 'LL_ESSAY';
$string['essayname'] = 'Essay';
$string['audiotextid'] = 'LL_AUDIO';
$string['audioname'] = 'Audio';
$string['videotextid'] = 'LL_VIDEO';
$string['videoname'] = 'Video';

/// question type instructions
$string[$string['descriptiontextid'].'instructions'] = 'Please read the instructions.';
$string[$string['multichoicetextid'].'instructions'] = 'Please select one answer.';
$string[$string['multichoicetextid'].'multipleinstructions'] = 'Please select the correct answers.';
$string[$string['truefalsetextid'].'instructions'] = 'Please select one answer.';
$string[$string['shortanswertextid'].'instructions'] = 'Please enter your answer in the box.';
$string[$string['clozetextid'].'instructions'] = 'Please fill in the boxes.';
$string[$string['matchingtextid'].'instructions'] = 'Please match the below pairs.';
$string[$string['numericaltextid'].'instructions'] = 'Please enter your answer in the box.';
$string[$string['essaytextid'].'instructions'] = 'Please enter your answer in the box.';
$string[$string['audiotextid'].'instructions'] = 'Please record an audio submission.';
$string[$string['videotextid'].'instructions'] = 'Please record a video submission.';

$string['submit'] = 'Submit';

$string['correct'] = 'Correct';
$string['incorrect'] = 'Incorrect';
$string['submitted'] = 'Submitted';
$string['waitingforfeedback'] = 'Your submission will be graded later.';

$string['iconsrccorrect'] = '/pix/i/tick_green_big.gif';
$string['iconsrcwrong'] = '/pix/i/cross_red_big.gif';
$string['iconsrcmanual'] = '/mod/languagelesson/icons/speaker.jpg';
$string['iconsrcfeedback'] = '/mod/languagelesson/icons/fb_balloon.jpg';

$string['feedbacktextframe'] = '$a->fullname said:<br />$a->text';

$string['teacherrecordswarning'] = 'Please be aware that no attempt data is recorded for teachers.';

$string['yousubmitted'] = 'You submitted:';

$string['submissionfeedback'] = 'Submission feedback';

$string['audioresponse'] = 'Audio response';
$string['feedbackplayerinstructions'] = 'Press play to hear your submission. Click on a speech bubble to hear the teacher\'s
feedback.';
$string['feedbackplayerinstructionssimple'] = 'Press play to hear the teacher\'s feedback';
$string['fbrecorderinstructions'] = 'Press play to hear the student\'s submission.  Press record to record your feedback at the current
time.';
$string['fbrecorderinstructionssimple'] = 'Please record your feedback';

$string['audiosubmissionstring'] = 'Student submission can be heard below.';
$string['studentsubmission'] = 'Student submission';

$string['casesensitive'] = 'Case sensitive';

$string['usedropdown'] = 'Use drop-down box for answer $a->number';

$string['nextstudent'] = 'Next student';
$string['nextquestion'] = 'Next question';
$string['previousquestion'] = 'Previous question';
$string['cancel'] = 'Cancel';

$string['emailssent'] = 'Notification emails sent';
$string['gradessaved'] = 'Custom grades assigned';

$string['add4moreanswerfields'] = 'Add 4 more answer fields';
$string['add4morebranches'] = 'Add 4 more branches';

$string['defaultpoints'] = 'Default points per question';

$string['branchtitle'] = 'Title of branch';
?>
